﻿using System;

namespace GtdCore.Services.Exceptions
{
	public class CantMoveBackwardsException : Exception
	{
		public CantMoveBackwardsException()
			: base("Youy cant move backwards you are allready at the beginning.")
		{ }
	}
}
