﻿using System;

namespace GtdCore.Services.Exceptions
{
	public class NoSuchStateException : Exception
	{
		public NoSuchStateException(int id)
			: base(string.Format("There is no state with id {0}", id)) { }
	}
}
