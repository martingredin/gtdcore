﻿using System;

namespace GtdCore.Services.Exceptions
{
	public class CantMoveForwardsException : Exception
	{
		public CantMoveForwardsException()
			: base("You cant move forwards you are allready at the end.")
		{ }
	}
}
