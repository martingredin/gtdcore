﻿using GtdCore.Core.Model;
using GtdCore.Core.Services;
using GtdCore.Infrastructure.Data;
using GtdCore.Services.Exceptions;
using Shared.Repositories;
using System.Collections.Generic;

namespace GtdCore.Services.Services
{
	public class StateService : IStateService
	{
		GtdContext context;

		public StateService(GtdContext context)
		{
			this.context = context;
		}

		public KanbanState GetState(int stateId)
		{
			var stateRepo = new EFRepository<KanbanState>(context);
			var state = stateRepo.GetById(stateId); ;

			if (state == null)
				throw new NoSuchStateException(stateId);

			return state;
		}

		public IEnumerable<KanbanState> GetAllStates()
		{
			var stateRepo = new EFRepository<KanbanState>(context);
			return stateRepo.List();
		}
	}
}