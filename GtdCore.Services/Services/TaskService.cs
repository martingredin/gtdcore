﻿using GtdCore.Core.Model;
using GtdCore.Core.Services;
using GtdCore.Infrastructure.Data;
using GtdCore.Infrastructure.Repositories;
using GtdCore.Services.Exceptions;
using Shared.Repositories;
using System;
using System.Collections.Generic;

namespace GtdCore.Services.Services
{
	public class TaskService : ITaskService
	{
		GtdContext context;

		public TaskService(GtdContext context)
		{
			this.context = context;
		}

		public GtdTask AddTask(string userName, string name, string description, DateTime? startDate, DateTime? endDate)
		{
			var userRepo = new UserRepository(context);
			var stateRepo = new StateRepository(context);
			var taskRepo = new EFRepository<GtdTask>(context);

			var user = userRepo.GetUser(userName);

			var task = GtdTask.Create(name, description, startDate, endDate);
			task.KanbanStateId = stateRepo.GetStateByName("New").Id;

			context.SaveChanges();

			user.Tasks.Add(task);
			context.GtdTasks.Add(task);

			int noRows = context.SaveChanges();

			return task;
		}

		public GtdTask UpdateTask(int taskId, string newName, string newDescription)
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			var task = taskRepo.GetById(taskId);
			task.Name = newName;
			task.Description = newDescription;
			context.SaveChanges();
			return task;
		}

		public string MoveTaskToState(int taskId, Direction direction)
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			var stateRepo = new EFRepository<KanbanState>(context);
			var task = taskRepo.GetById(taskId);
			var state = stateRepo.GetById((int)task.KanbanStateId);

			switch (direction)
			{
				case Direction.Backwards:
					if (state.PreviousStateId == null)
						throw new CantMoveBackwardsException();

					task.KanbanStateId = state.PreviousStateId;
					break;

				case Direction.Forwards:
					if (state.NextStateId == null)
						throw new CantMoveForwardsException();

					task.KanbanStateId = state.NextStateId;
					break;
			}

			context.SaveChanges();

			return stateRepo.GetById((int)task.KanbanStateId).Name;
		}

		public void ChangeTaskToState(int taskId, int stateId)
		{
			var stateRepo = new EFRepository<KanbanState>(context);
			var taskRepo = new EFRepository<GtdTask>(context);

			var newState = stateRepo.GetById(stateId);
			var task = taskRepo.GetById(taskId);

			task.KanbanStateId = newState.Id;
			context.SaveChanges();
		}

		public GtdTask GetTask(int taskId)
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			return taskRepo.GetById(taskId);
		}

		public IEnumerable<GtdTask> GetAllTasks()
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			return taskRepo.List();
		}

		public IEnumerable<GtdTask> GetTasksForUser(string userName)
		{
			var userRepo = new UserRepository(context);
			var taskRepo = new TaskRepository(context);

			var user = userRepo.GetUser(userName);
			var tasks = taskRepo.GetTaskByUserId(user.Id);

			return tasks;
		}

		public int DeleteTask(int taskId)
		{
			var taskRepo = new EFRepository<GtdTask>(context);
			var task = taskRepo.GetById(taskId);
			taskRepo.Remove(task);
			context.SaveChanges();
			return taskId;
		}
	}
}