﻿using GtdCore.Core.Model;
using System.Collections.Generic;

namespace GtdCore.Core.Services
{
    public interface IStateService
    {
        KanbanState GetState(int stateId);

        IEnumerable<KanbanState> GetAllStates();
    }
}
