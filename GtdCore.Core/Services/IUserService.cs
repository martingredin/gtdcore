﻿using GtdCore.Core.Model;

namespace GtdCore.Core.Services
{
    public interface IUserService
    {
        bool AuthenticateUser(string userName, string password);
        bool RegisterUser(string userName, string password, string confirmPassword);
        User GetUser(string userName);
    }
}
