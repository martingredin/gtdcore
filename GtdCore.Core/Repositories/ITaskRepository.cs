﻿using GtdCore.Core.Model;
using System.Collections.Generic;

namespace GtdCore.Core.Repositories
{
	public interface ITaskRepository
	{
		IEnumerable<GtdTask> GetTaskByUserId(int userId);
	}
}
