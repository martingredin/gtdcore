﻿using GtdCore.Core.Model;

namespace GtdCore.Core.Repositories
{
    public interface IStateRepository
    {
        KanbanState GetStateByName(string name);
    }
}