﻿using GtdCore.Core.Model;

namespace GtdCore.Core.Repositories
{
    public interface IUserRepository
    {
        User GetUser(string userName);
    }
}
