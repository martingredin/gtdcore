﻿using Shared.Model;

namespace GtdCore.Core.Model
{
    public class GtdSubTask : Entity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public int GtdTaskId { get; set; }
        public GtdTask GtdTask { get; set; }
    }
}
