﻿using Shared.Model;

namespace GtdCore.Core.Model
{
    public class KanbanState : Entity<int>
    {
        public string Name { get; set; }
        public int? PreviousStateId { get; set; }
        public int? NextStateId { get; set; }
    }
}
