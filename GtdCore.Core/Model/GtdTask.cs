﻿using Shared.Model;
using System;
using System.Collections.Generic;

namespace GtdCore.Core.Model
{
    public class GtdTask : Entity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public List<GtdSubTask> SubTasks { get; set; }

        public int? KanbanStateId { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public GtdTask()
        {
            SubTasks = new List<GtdSubTask>();
        }

        public static GtdTask Create(string name, string description, DateTime? startDate = null, DateTime? endDate = null)
        {
            return new GtdTask()
            {
                Name = name,
                Description = description,
                StartDate = startDate,
                EndDate = endDate
            };
        }

        public void AddSubTask(GtdSubTask task)
        {
            SubTasks.Add(task);
        }
    }
}
