﻿using GtdCore.Core.Model;
using GtdCore.Core.Repositories;
using GtdCore.Infrastructure.Data;
using Shared.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace GtdCore.Infrastructure.Repositories
{
	public class TaskRepository : EFRepository<GtdTask>, ITaskRepository
	{
		protected GtdContext GtdContext
		{
			get { return (GtdContext)DataContext; }
		}

		public TaskRepository(GtdContext context)
			: base(context)
		{

		}

		public IEnumerable<GtdTask> GetTaskByUserId(int userId)
		{
			return (from t in GtdContext.GtdTasks
					where t.UserId == userId
					select t).ToList();
		}
	}
}
