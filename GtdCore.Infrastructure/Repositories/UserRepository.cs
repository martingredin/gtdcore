﻿using GtdCore.Core.Model;
using GtdCore.Core.Repositories;
using GtdCore.Infrastructure.Data;
using Shared.Repositories;
using System.Linq;

namespace GtdCore.Infrastructure.Repositories
{
	public class UserRepository : EFRepository<User>, IUserRepository
	{
		protected GtdContext GtdContext
		{
			get { return (GtdContext)DataContext; }
		}

		public UserRepository(GtdContext context)
			: base(context)
		{

		}

		public User GetUser(string userName)
		{
			return (from u in GtdContext.Users
					where u.UserName == userName
					select u).FirstOrDefault();
		}
	}
}
