﻿using GtdCore.Core.Model;
using GtdCore.Core.Repositories;
using GtdCore.Infrastructure.Data;
using Shared.Repositories;
using System.Linq;

namespace GtdCore.Infrastructure.Repositories
{
	public class StateRepository : EFRepository<KanbanState>, IStateRepository
	{
		protected GtdContext GtdContext
		{
			get { return (GtdContext)DataContext; }
		}

		public StateRepository(GtdContext context)
			: base(context)
		{

		}

		public KanbanState GetStateByName(string name)
		{
			return (from s in GtdContext.KanbaanStates
					where s.Name == name
					select s).FirstOrDefault();
		}
	}
}
