﻿using GtdCore.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace GtdCore.Infrastructure.Data
{
	public class GtdContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<GtdTask> GtdTasks { get; set; }
        public virtual DbSet<KanbanState> KanbaanStates { get; set; }

		

		public GtdContext(DbContextOptions<GtdContext> options) : base(options)
        {

		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
			//optionsBuilder.UseSqlServer("Server=localhost;Database=GtdDb;Trusted_Connection=True;");
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			//modelBuilder.Entity<KanbaanState>()
			//	.HasOptional(e => e.NextState)
			//	.WithMany()
			//	.HasForeignKey(m => m.NextStateId);

			//modelBuilder.Entity<KanbaanState>()
			//	.HasOptional(e => e.PreviousState)
			//	.WithMany()
			//	.HasForeignKey(m => m.PreviousStateId);

			//modelBuilder.Entity<KanbaanState>()
			//	.HasOptional(t => t.GtdTask)
			//	.WithRequired(s => s.KanbaanState);

			//modelBuilder.Entity<GtdTask>()
			//	.HasRequired(r => r.KanbaanState)
			//	.WithOptional(s => s.GtdTask);

			//modelBuilder.Entity<GtdTask>()
			//	.HasKey(k => k.Id)
			//	.Property(p => p.Id)
			//	.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);



			modelBuilder.Entity<GtdTask>()
                .HasMany(m => m.SubTasks)
                .WithOne(m => m.GtdTask)
                .IsRequired();

            modelBuilder.Entity<User>()
                .HasMany(r => r.Tasks)
                .WithOne(r => r.User)
                .IsRequired();
        }
    }
}
