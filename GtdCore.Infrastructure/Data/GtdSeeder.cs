﻿using GtdCore.Core.Model;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace GtdCore.Infrastructure.Data
{
	public class GtdSeeder
    {
		private readonly GtdContext ctx;

		private KanbanState newState;
		private KanbanState analyzedState;
		private KanbanState inProgressState;
		private KanbanState finishedState;

		public GtdSeeder(GtdContext ctx)
		{
			this.ctx = ctx;
		}

		public void Seed()
		{
			ctx.Database.EnsureCreated();

			if (!ctx.KanbaanStates.Any())
			{
				newState = new KanbanState { Name = "New" };
				analyzedState = new KanbanState { Name = "Analyzed" };
				inProgressState = new KanbanState { Name = "In progress" };
				finishedState = new KanbanState { Name = "Finished" };

				ctx.KanbaanStates.Add(newState);
				ctx.KanbaanStates.Add(analyzedState);
				ctx.KanbaanStates.Add(inProgressState);
				ctx.KanbaanStates.Add(finishedState);
				ctx.SaveChanges();

				newState.NextStateId = analyzedState.Id;
				analyzedState.PreviousStateId = newState.Id;
				analyzedState.NextStateId = inProgressState.Id;
				inProgressState.PreviousStateId = analyzedState.Id;
				inProgressState.NextStateId = finishedState.Id;
				finishedState.PreviousStateId = inProgressState.Id;
				ctx.SaveChanges();
			}

			if (!ctx.Users.Any())
			{
				RNGCryptoServiceProvider rngCryptoSP = new RNGCryptoServiceProvider();
				byte[] saltArr = new byte[10];

				rngCryptoSP.GetBytes(saltArr);
				var salt1 = Convert.ToBase64String(saltArr);
				rngCryptoSP.GetBytes(saltArr);
				var salt2 = Convert.ToBase64String(saltArr);

				ctx.Users.AddRange(new[] {
					User.Create("Martin", "abc123", salt1),
					User.Create("Kalle", "cde345", salt2)
				});

				ctx.SaveChanges();
			}
		}
	}
}
