﻿using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using GtdCore.Core.Repositories;
using GtdCore.Core.Services;
using GtdCore.Infrastructure.Data;
using GtdCore.Infrastructure.Repositories;
using GtdCore.Services.Services;
using GtdCore.Web.Models;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace GtdCore.Web
{
	public class Startup
	{
		public IConfiguration Configuration { get; }

		public static readonly LoggerFactory ConsoleLoggerFactory = new LoggerFactory(new[] { new ConsoleLoggerProvider((_, __) => true, true) });

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			//Entity framework
			services.AddDbContext<GtdContext>(cfg =>
			{
				//var connstring = Configuration.GetConnectionString("GtdConnectionString");
				cfg.UseSqlServer(Configuration.GetConnectionString("GtdConnectionString"));
				cfg.UseLoggerFactory(ConsoleLoggerFactory);
			});
			services.AddTransient<GtdSeeder>();

			//Services
			services.AddScoped<IStateService, StateService>();
			services.AddScoped<ITaskService, TaskService>();
			services.AddScoped<IUserService, UserService>();

			//JWT token authentication
			JwtSettings jwtSettings = new JwtSettings
			{
				Key = Configuration["JwtSettings:key"],
				Audience = Configuration["JwtSettings:audience"],
				Issuer = Configuration["JwtSettings:issuer"],
				MinutesToExpiration = Convert.ToInt32(Configuration["JwtSettings:minutesToExpiration"])
			};
			services.AddSingleton<JwtSettings>(jwtSettings);

			services
				.AddAuthentication(options =>
				{
					options.DefaultAuthenticateScheme = "JwtBearer";
					options.DefaultChallengeScheme = "JwtBearer";
				})
				.AddJwtBearer("JwtBearer", jwtBearerOptions =>
				{
					jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
					{
						ValidateIssuerSigningKey = true,
						IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Key)),
						ValidateIssuer = true,
						ValidIssuer = jwtSettings.Issuer,

						ValidateAudience = true,
						ValidAudience = jwtSettings.Audience,

						ValidateLifetime = true,
						ClockSkew = TimeSpan.FromMinutes(jwtSettings.MinutesToExpiration)
					};
				});

			services
				.AddMvc()
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
				.AddJsonOptions(options =>
					options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());

		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseStaticFiles();
			app.UseAuthentication();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller}/{action=Index}/{id?}");
			});
		}
	}
}
