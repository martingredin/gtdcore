﻿namespace GtdCore.Web.Models
{
	public class MoveTaskDto
	{
		public int TaskId { get; set; }
		public int Direction { get; set; }
	}
}