﻿namespace GtdCore.Web.Models
{
	public class UserModelDto
	{
		public string UserId { get; set; }
		public string Password { get; set; }
		public string ConfirmPassword { get; set; }
	}
}