﻿namespace GtdCore.Web.Models
{
	public class TaskDto
	{
		public int Id { get; set; }
		public string UserName { get; set; }
		public string TaskName { get; set; }
		public string Description { get; set; }
		public string State { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
	}
}