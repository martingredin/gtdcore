﻿using System;

namespace GtdCore.Web.Models
{
	public class TokenDto
	{
		public string Token { get; set; }
		public DateTime ExpiresIn { get; set; }
	}
}
