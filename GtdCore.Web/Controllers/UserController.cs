﻿using GtdCore.Web.Models;
using GtdCore.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GtdCore.Web.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
		private IUserService userService;

		public UserController(IUserService userService)
		{
			this.userService = userService;
		}

		[HttpPost]
		[AllowAnonymous]
		[ActionName("Register")]
		public IActionResult Register([FromBody]UserModelDto userModel)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			if (!userService.RegisterUser(userModel.UserId, userModel.Password, userModel.ConfirmPassword))
				return BadRequest();

			return Ok();
		}
	}
}