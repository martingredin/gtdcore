﻿using System;
using System.Collections.Generic;
using GtdCore.Web.Models;
using GtdCore.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace GtdCore.Web.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
		private ITaskService taskService;
		private IUserService userService;
		private IStateService stateService;

		public TaskController(ITaskService taskService, IUserService userService, IStateService stateService)
		{
			this.taskService = taskService;
			this.userService = userService;
			this.stateService = stateService;
		}

		[HttpGet]
		[Authorize]
		public IActionResult GetTasksByUser(string userName)
		{
			try
			{
				var tasks = taskService.GetTasksForUser(userName);

				var dtos = new List<TaskDto>();
				foreach (var task in tasks)
				{
					var dto = new TaskDto
					{
						Id = task.Id,
						UserName = userName,
						TaskName = task.Name,
						Description = task.Description,
						StartDate = ((DateTime)task.StartDate).ToString("yyyy-MM-dd"),
						EndDate = ((DateTime)task.EndDate).ToString("yyyy-MM-dd")
					};

					dto.State = stateService.GetState((int)task.KanbanStateId).Name;

					dtos.Add(dto);
				}

				return StatusCode(StatusCodes.Status200OK, dtos.ToArray());
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
			}
		}

		[HttpPost]
		[Authorize]
		public IActionResult CreateTask(TaskDto dto)
		{
			try
			{
				DateTime? startDate = null;
				if (!string.IsNullOrEmpty(dto.StartDate))
					startDate = DateTime.Parse(dto.StartDate);

				DateTime? endDate = null;
				if (!string.IsNullOrEmpty(dto.EndDate))
					endDate = DateTime.Parse(dto.EndDate);

				var newTask = taskService.AddTask(dto.UserName, dto.TaskName, dto.Description, startDate, endDate);

				return StatusCode(StatusCodes.Status200OK,
					new TaskDto
					{
						Id = newTask.Id,
						UserName = dto.UserName,
						TaskName = newTask.Name,
						Description = newTask.Description,
						State = stateService.GetState((int)newTask.KanbanStateId).Name,
						StartDate = newTask.StartDate != null ? ((DateTime)newTask.StartDate).ToString("yyyy-MM-dd") : "",
						EndDate = newTask.EndDate != null ? ((DateTime)newTask.EndDate).ToString("yyyy-MM-dd") : ""
					});
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
			}
		}

		[HttpPut]
		[Authorize]
		public IActionResult EditTask(TaskDto dto)
		{
			try
			{
				var updatedTask = taskService.UpdateTask(dto.Id, dto.TaskName, dto.Description);

				return StatusCode(StatusCodes.Status200OK,
					new TaskDto
					{
						Id = updatedTask.Id,
						UserName = dto.UserName,
						TaskName = updatedTask.Name,
						Description = updatedTask.Description,
						State = stateService.GetState((int)updatedTask.KanbanStateId).Name,
						StartDate = updatedTask.StartDate != null ? ((DateTime)updatedTask.StartDate).ToString("yyyy-MM-dd") : "",
						EndDate = updatedTask.EndDate != null ? ((DateTime)updatedTask.EndDate).ToString("yyyy-MM-dd") : ""
					});
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
			}
		}

		[HttpPatch]
		[Authorize]
		public IActionResult MoveTask(MoveTaskDto dto)
		{
			try
			{
				Direction d = (Direction)dto.Direction;
				var newStateName = taskService.MoveTaskToState(dto.TaskId, d);
				return StatusCode(StatusCodes.Status200OK, new TaskDto { Id = dto.TaskId, State = newStateName });
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
			}
		}

		[HttpDelete]
		[Authorize]
		public IActionResult DeleteTask(int taskId)
		{
			try
			{
				int removedTaskId = taskService.DeleteTask(taskId);
				return StatusCode(StatusCodes.Status200OK, removedTaskId);
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
			}
		}
	}
}