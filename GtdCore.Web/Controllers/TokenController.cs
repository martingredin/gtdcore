﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using GtdCore.Core.Services;
using GtdCore.Web.Models;

namespace GtdCore.Web.Controllers
{
	public class TokenController : Controller
    {
		private IUserService userService;
		private JwtSettings jwtSettings;

		public TokenController(IUserService userService, JwtSettings jwtSettings)
		{
			this.userService = userService;
			this.jwtSettings = jwtSettings;
		}

		[AllowAnonymous]
		[Route("api/token")]
		[HttpPost]
		public IActionResult Token([FromBody]LoginDto model)
		{
			if (!ModelState.IsValid)
				return StatusCode(StatusCodes.Status400BadRequest, "Invalid User Name/Password.");

			if (!userService.AuthenticateUser(model.Username, model.Password))
				return StatusCode(StatusCodes.Status401Unauthorized, "Invalid User Name/Password");

			SymmetricSecurityKey encryptionKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Key));
			List<Claim> jwtClaims = new List<Claim>();
			jwtClaims.Add(new Claim(JwtRegisteredClaimNames.Sub, model.Username));
			jwtClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));

			var token = new JwtSecurityToken(
				issuer: jwtSettings.Issuer,
				audience: jwtSettings.Audience,
				claims: jwtClaims,
				notBefore: DateTime.Now,
				expires: DateTime.Now.AddMinutes(jwtSettings.MinutesToExpiration),
				signingCredentials: new SigningCredentials(encryptionKey, SecurityAlgorithms.HmacSha256)
			);

			var tokenString = "{ token: " + new JwtSecurityTokenHandler().WriteToken(token) + "}";

			TokenDto t = new TokenDto
			{
				Token = new JwtSecurityTokenHandler().WriteToken(token),
				ExpiresIn = token.ValidTo
			};


			return StatusCode(StatusCodes.Status200OK, t);
		}
	}
}