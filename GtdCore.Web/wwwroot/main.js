(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/LoginDto.ts":
/*!*****************************!*\
  !*** ./src/app/LoginDto.ts ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// Created with Typewriter (http://frhagn.github.io/Typewriter/)
Object.defineProperty(exports, "__esModule", { value: true });
var LoginDto = /** @class */ (function () {
    function LoginDto(data) {
        if (data === void 0) { data = null; }
        // USERNAME
        this.username = null;
        // PASSWORD
        this.password = null;
        if (data !== null) {
            this.username = data.username;
            this.password = data.password;
        }
    }
    return LoginDto;
}());
exports.LoginDto = LoginDto;


/***/ }),

/***/ "./src/app/MoveTaskDto.ts":
/*!********************************!*\
  !*** ./src/app/MoveTaskDto.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// Created with Typewriter (http://frhagn.github.io/Typewriter/)
Object.defineProperty(exports, "__esModule", { value: true });
var MoveTaskDto = /** @class */ (function () {
    function MoveTaskDto(data) {
        if (data === void 0) { data = null; }
        // TASKID
        this.taskId = 0;
        // DIRECTION
        this.direction = 0;
        if (data !== null) {
            this.taskId = data.taskId;
            this.direction = data.direction;
        }
    }
    return MoveTaskDto;
}());
exports.MoveTaskDto = MoveTaskDto;


/***/ }),

/***/ "./src/app/TaskDto.ts":
/*!****************************!*\
  !*** ./src/app/TaskDto.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// Created with Typewriter (http://frhagn.github.io/Typewriter/)
Object.defineProperty(exports, "__esModule", { value: true });
var TaskDto = /** @class */ (function () {
    function TaskDto(data) {
        if (data === void 0) { data = null; }
        // ID
        this.id = 0;
        // USERNAME
        this.userName = null;
        // TASKNAME
        this.taskName = null;
        // DESCRIPTION
        this.description = null;
        // STATE
        this.state = null;
        // STARTDATE
        this.startDate = null;
        // ENDDATE
        this.endDate = null;
        if (data !== null) {
            this.id = data.id;
            this.userName = data.userName;
            this.taskName = data.taskName;
            this.description = data.description;
            this.state = data.state;
            this.startDate = data.startDate;
            this.endDate = data.endDate;
        }
    }
    return TaskDto;
}());
exports.TaskDto = TaskDto;


/***/ }),

/***/ "./src/app/UserModelDto.ts":
/*!*********************************!*\
  !*** ./src/app/UserModelDto.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// Created with Typewriter (http://frhagn.github.io/Typewriter/)
Object.defineProperty(exports, "__esModule", { value: true });
var UserModelDto = /** @class */ (function () {
    function UserModelDto(data) {
        if (data === void 0) { data = null; }
        // USERID
        this.userId = null;
        // PASSWORD
        this.password = null;
        // CONFIRMPASSWORD
        this.confirmPassword = null;
        if (data !== null) {
            this.userId = data.userId;
            this.password = data.password;
            this.confirmPassword = data.confirmPassword;
        }
    }
    return UserModelDto;
}());
exports.UserModelDto = UserModelDto;


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var route_guard_1 = __webpack_require__(/*! ./auth/route.guard */ "./src/app/auth/route.guard.ts");
var front_page_component_1 = __webpack_require__(/*! ./front-page/front-page.component */ "./src/app/front-page/front-page.component.ts");
var task_mgmt_component_1 = __webpack_require__(/*! ./task-mgmt/task-mgmt.component */ "./src/app/task-mgmt/task-mgmt.component.ts");
var state_mgmt_component_1 = __webpack_require__(/*! ./state-mgmt/state-mgmt.component */ "./src/app/state-mgmt/state-mgmt.component.ts");
var routes = [
    {
        path: '',
        redirectTo: '/front',
        pathMatch: 'full'
    },
    {
        path: 'front',
        component: front_page_component_1.FrontPageComponent
    },
    {
        path: 'taskMgmt',
        component: task_mgmt_component_1.TaskMgmtComponent,
        canActivate: [route_guard_1.RouteGuard]
    },
    {
        path: 'stateMgmt',
        component: state_mgmt_component_1.StateMgmtComponent,
        canActivate: [route_guard_1.RouteGuard]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\r\n        <a class=\"navbar-brand\" routerLink=\"/front\" routerLinkActive=\"active\">GTD</a>       \r\n        <div id=\"navbar\"  class=\"ml-auto\">\r\n            <auth></auth>\r\n        </div>\r\n\r\n    </nav>\r\n</div>\r\n\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var bootstrap_1 = __webpack_require__(/*! ngx-modialog/plugins/bootstrap */ "./node_modules/ngx-modialog/plugins/bootstrap/fesm5/ngx-modialog-plugins-bootstrap.js");
var dialog_service_1 = __webpack_require__(/*! ./dialogs/dialog.service */ "./src/app/dialogs/dialog.service.ts");
var auth_service_1 = __webpack_require__(/*! ./auth/auth.service */ "./src/app/auth/auth.service.ts");
var TaskDto_1 = __webpack_require__(/*! ./TaskDto */ "./src/app/TaskDto.ts");
var AppComponent = /** @class */ (function () {
    function AppComponent(modal, dialogService, authService) {
        this.modal = modal;
        this.dialogService = dialogService;
        this.authService = authService;
        this.title = 'gtd-client';
    }
    AppComponent.prototype.showDlg = function () {
        var dialog = this.modal.confirm()
            .okBtn("Yes")
            .cancelBtn("No")
            .size('sm')
            .showClose(true)
            .title('Confirm')
            .body("Do you want to accept?")
            .open();
        dialog.result.then(function (res) { console.log("res: " + res); }, function (err) { console.log("rejected"); });
    };
    AppComponent.prototype.showInfoDlg = function () {
        this.dialogService
            .showInformationDialog('info heading', 'info body')
            .result.then(function (res) { return console.log("ok:"); }, function (err) { return console.log("cancel: " + err); });
    };
    AppComponent.prototype.showErrorRepDlg = function () {
        this.dialogService
            .showErrorReportDialog()
            .result.then(function (res) { return console.log("ok:\n" + res.name + '\n' + res.phoneNumber + '\n' + res.email + '\n' + res.description); }, function (err) { return console.log("cancel: " + err); });
    };
    AppComponent.prototype.showRegForm = function () {
        this.dialogService
            .showRegDialog()
            .result.then(function (res) { return console.log("ok:\n" + res.userId + '\n' + res.password + '\n' + res.confirmPassword); }, function (err) { return console.log("rejected: " + err); });
    };
    AppComponent.prototype.showTCDlg = function () {
        this.dialogService
            .showTaskCreateDialog()
            .result.then(function (res) { return console.log("ok:\n" + res.taskName + '\n' + res.description + '\n' + res.startDate + '\n' + res.endDate); }, function (err) { return console.log("rejected: " + err); });
    };
    AppComponent.prototype.showTEDlg = function () {
        var taskDto = new TaskDto_1.TaskDto();
        taskDto.id = 55;
        taskDto.taskName = 'afsaefsdf';
        taskDto.userName = 'asdfcasfcs';
        taskDto.description = 'vdfhgyhty';
        taskDto.startDate = '2018-10-10';
        taskDto.endDate = '2019-01-01';
        this.dialogService
            .showTaskEditDialog(taskDto)
            .result.then(function (res) { return console.log("ok:\n" + res.taskName + '\n' + res.description + '\n' + res.startDate + '\n' + res.endDate); }, function (err) { return console.log("rejected: " + err); });
    };
    AppComponent.prototype.logIn = function () {
        console.log("entering login");
        this.authService.login("Olle", "fgh321")
            .subscribe(function (res) {
            console.log('success: ' + res);
        }), function (err) {
            console.log("Error Occured " + err);
        };
    };
    AppComponent.prototype.isLoggedIn = function () {
        if (this.authService.isLoggedIn()) {
            var token = this.authService.getToken();
            this.dialogService.showInformationDialog('isLoggedIn s�ger att', 'vi �r inloggade med token: ' + token);
        }
        else
            this.dialogService.showInformationDialog('isLoggedIn s�ger att', 'vi inte �r inloggade');
    };
    AppComponent.prototype.logOut = function () {
        this.authService.logOut();
        this.dialogService.showInformationDialog('', 'du �r nu utloggad');
    };
    AppComponent.prototype.register = function () {
        var _this = this;
        this.dialogService.showRegDialog()
            .result.then(function (returnData) {
            _this.authService.createUser(returnData)
                .subscribe(function (res) {
                _this.dialogService.showInformationDialog('Welcome to the Gtd community', 'Thank you for joining our website, we hope youe life will become more efficient and productive with our help. An email with a confirmation link has been sent to the email address you supplied, please click the link to activate your registration before trying to login.');
            }), function (err) {
                _this.dialogService.showInformationDialog('Registration failed', 'Error message: ' + err);
            };
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [bootstrap_1.Modal,
            dialog_service_1.DialogService,
            auth_service_1.AuthService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var app_routing_module_1 = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
var app_component_1 = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var ngx_modialog_1 = __webpack_require__(/*! ngx-modialog */ "./node_modules/ngx-modialog/fesm5/ngx-modialog.js");
var bootstrap_1 = __webpack_require__(/*! ngx-modialog/plugins/bootstrap */ "./node_modules/ngx-modialog/plugins/bootstrap/fesm5/ngx-modialog-plugins-bootstrap.js");
var ng2_datepicker_1 = __webpack_require__(/*! ng2-datepicker */ "./node_modules/ng2-datepicker/dist/bundles/ng2-datepicker.umd.js");
var ngx_webstorage_1 = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/fesm5/ngx-webstorage.js");
var angular_font_awesome_1 = __webpack_require__(/*! angular-font-awesome */ "./node_modules/angular-font-awesome/dist/angular-font-awesome.es5.js");
var dialog_service_1 = __webpack_require__(/*! ./dialogs/dialog.service */ "./src/app/dialogs/dialog.service.ts");
var information_dialog_component_1 = __webpack_require__(/*! ./dialogs/information-dialog/information-dialog.component */ "./src/app/dialogs/information-dialog/information-dialog.component.ts");
var error_report_dialog_component_1 = __webpack_require__(/*! ./dialogs/error-report-dialog/error-report-dialog.component */ "./src/app/dialogs/error-report-dialog/error-report-dialog.component.ts");
var registration_form_component_1 = __webpack_require__(/*! ./dialogs/registration-form/registration-form.component */ "./src/app/dialogs/registration-form/registration-form.component.ts");
var task_create_dialog_component_1 = __webpack_require__(/*! ./dialogs/task-create-dialog/task-create-dialog.component */ "./src/app/dialogs/task-create-dialog/task-create-dialog.component.ts");
var task_edit_dialog_component_1 = __webpack_require__(/*! ./dialogs/task-edit-dialog/task-edit-dialog.component */ "./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.ts");
var auth_service_1 = __webpack_require__(/*! ./auth/auth.service */ "./src/app/auth/auth.service.ts");
var auth_component_1 = __webpack_require__(/*! ./auth/auth/auth.component */ "./src/app/auth/auth/auth.component.ts");
var http_interceptor_module_1 = __webpack_require__(/*! ./auth/http-interceptor.module */ "./src/app/auth/http-interceptor.module.ts");
var front_page_component_1 = __webpack_require__(/*! ./front-page/front-page.component */ "./src/app/front-page/front-page.component.ts");
var task_mgmt_component_1 = __webpack_require__(/*! ./task-mgmt/task-mgmt.component */ "./src/app/task-mgmt/task-mgmt.component.ts");
var task_service_1 = __webpack_require__(/*! ./task-mgmt/task.service */ "./src/app/task-mgmt/task.service.ts");
var state_mgmt_component_1 = __webpack_require__(/*! ./state-mgmt/state-mgmt.component */ "./src/app/state-mgmt/state-mgmt.component.ts");
var state_filter_pipe_1 = __webpack_require__(/*! ./state-mgmt/state-filter.pipe */ "./src/app/state-mgmt/state-filter.pipe.ts");
bootstrap_1.bootstrap4Mode();
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                information_dialog_component_1.InformationDialogComponent,
                error_report_dialog_component_1.ErrorReportDialogComponent,
                registration_form_component_1.RegistrationFormComponent,
                task_create_dialog_component_1.TaskCreateDialogComponent,
                task_edit_dialog_component_1.TaskEditDialogComponent,
                front_page_component_1.FrontPageComponent,
                auth_component_1.AuthComponent,
                task_mgmt_component_1.TaskMgmtComponent,
                state_mgmt_component_1.StateMgmtComponent,
                state_filter_pipe_1.StateFilterPipe
            ],
            entryComponents: [
                information_dialog_component_1.InformationDialogComponent,
                error_report_dialog_component_1.ErrorReportDialogComponent,
                registration_form_component_1.RegistrationFormComponent,
                task_create_dialog_component_1.TaskCreateDialogComponent,
                task_edit_dialog_component_1.TaskEditDialogComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                ngx_modialog_1.ModalModule.forRoot(),
                bootstrap_1.BootstrapModalModule,
                ng2_datepicker_1.NgDatepickerModule,
                ngx_webstorage_1.NgxWebstorageModule.forRoot(),
                angular_font_awesome_1.AngularFontAwesomeModule,
                http_interceptor_module_1.HttpInterceptorModule
            ],
            providers: [
                dialog_service_1.DialogService,
                auth_service_1.AuthService,
                task_service_1.TaskService
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var rxjs_1 = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var operators_1 = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var ngx_webstorage_1 = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/fesm5/ngx-webstorage.js");
var authentication_1 = __webpack_require__(/*! ./authentication */ "./src/app/auth/authentication.ts");
var globals_1 = __webpack_require__(/*! ../globals */ "./src/app/globals.ts");
var LoginDto_1 = __webpack_require__(/*! ../LoginDto */ "./src/app/LoginDto.ts");
var dialog_service_1 = __webpack_require__(/*! ../dialogs/dialog.service */ "./src/app/dialogs/dialog.service.ts");
var AuthService = /** @class */ (function () {
    function AuthService(http, localStorageService, dialogService) {
        this.http = http;
        this.localStorageService = localStorageService;
        this.dialogService = dialogService;
        this.loginUrl = globals_1.Globals.BASE_API_URL + '/token';
        this.regUrl = globals_1.Globals.BASE_API_URL + '/User';
        this.httpOptions = {
            headers: new http_1.HttpHeaders({ 'Content-Type': 'application/json' })
        };
    }
    AuthService.prototype.createUser = function (userModelDto) {
        return this.http
            .post(this.regUrl, userModelDto, this.httpOptions)
            .pipe(operators_1.tap(function (res) { return console.log('success: ' + res); }), operators_1.catchError(this.handleError('Authentication failed')));
    };
    AuthService.prototype.login = function (userName, password) {
        var _this = this;
        console.log("entering AuthService.login");
        console.log("loginUrl: " + this.loginUrl);
        var login = new LoginDto_1.LoginDto();
        login.username = userName;
        login.password = password;
        return this.http
            .post(this.loginUrl, login, this.httpOptions)
            .pipe(operators_1.tap(function (tokenDto) {
            console.log("fick ett token: " + tokenDto.token);
            var authentication = new authentication_1.Authentication();
            authentication.userName = login.username;
            authentication.token = String(tokenDto.token);
            authentication.expirationDate = new Date(tokenDto.expiresIn);
            _this.localStorageService.store(globals_1.Globals.AUTH_KEY, authentication);
        }), operators_1.catchError(this.handleError("POST: " + this.loginUrl)));
    };
    AuthService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            //TODO: send the error to remote logging infrastructure
            console.log(error); // log to console instead
            //throw an error to run the error function in subscribe
            return rxjs_1.throwError(error.error);
        };
    };
    AuthService.prototype.logOut = function () {
        this.localStorageService.clear(globals_1.Globals.AUTH_KEY);
    };
    AuthService.prototype.isLoggedIn = function () {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        var now = new Date();
        if (authentication == null || now > new Date(authentication.expirationDate))
            return false;
        return true;
    };
    AuthService.prototype.getToken = function () {
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        if (authentication)
            return authentication.token;
        return "";
    };
    AuthService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient,
            ngx_webstorage_1.LocalStorageService,
            dialog_service_1.DialogService])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;


/***/ }),

/***/ "./src/app/auth/auth/auth.component.css":
/*!**********************************************!*\
  !*** ./src/app/auth/auth/auth.component.css ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvYXV0aC9hdXRoLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/auth/auth/auth.component.html":
/*!***********************************************!*\
  !*** ./src/app/auth/auth/auth.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"form-inline\" (ngSubmit)=\"login()\" >\r\n    <div *ngIf=\"!loggedIn()\">\r\n        <input type=\"text\" placeholder=\"Username\"\r\n                [(ngModel)]=\"loginData.userId\"\r\n                name=\"Username\"\r\n                required style=\"margin-right:5px\" />\r\n        <input type=\"password\" placeholder=\"Password\"\r\n                [(ngModel)]=\"loginData.password\"\r\n                name=\"Password\"\r\n                required style=\"margin-right:5px\" />\r\n        <button type=\"submit\" class=\"btn btn-success\" style=\"margin-right:5px\">\r\n            <fa name=\"sign-in\"></fa>\r\n            Sign in\r\n        </button>\r\n    </div>\r\n\r\n    <div *ngIf=\"loggedIn()\">\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"logout()\">\r\n            <fa name=\"sign-out\"></fa>\r\n            Logout\r\n        </button>\r\n    </div>\r\n</form>"

/***/ }),

/***/ "./src/app/auth/auth/auth.component.ts":
/*!*********************************************!*\
  !*** ./src/app/auth/auth/auth.component.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var auth_service_1 = __webpack_require__(/*! ../auth.service */ "./src/app/auth/auth.service.ts");
var UserModelDto_1 = __webpack_require__(/*! ../../UserModelDto */ "./src/app/UserModelDto.ts");
var dialog_service_1 = __webpack_require__(/*! ../../dialogs/dialog.service */ "./src/app/dialogs/dialog.service.ts");
var AuthComponent = /** @class */ (function () {
    function AuthComponent(authService, router, dialogService) {
        this.authService = authService;
        this.router = router;
        this.dialogService = dialogService;
    }
    AuthComponent.prototype.ngOnInit = function () {
        this.loginData = new UserModelDto_1.UserModelDto();
    };
    AuthComponent.prototype.login = function () {
        var _this = this;
        this.authService.login(this.loginData.userId, this.loginData.password)
            .subscribe(function (success) {
            _this.router.navigateByUrl('/taskMgmt');
            _this.loginData = new UserModelDto_1.UserModelDto();
        }, function (err) {
            _this.dialogService.showInformationDialog("Login failed", err);
            _this.loginData = new UserModelDto_1.UserModelDto();
        });
    };
    AuthComponent.prototype.logout = function () {
        this.authService.logOut();
        this.router.navigateByUrl('/front');
    };
    AuthComponent.prototype.loggedIn = function () {
        return this.authService.isLoggedIn();
    };
    AuthComponent = __decorate([
        core_1.Component({
            selector: 'auth',
            template: __webpack_require__(/*! ./auth.component.html */ "./src/app/auth/auth/auth.component.html"),
            styles: [__webpack_require__(/*! ./auth.component.css */ "./src/app/auth/auth/auth.component.css")]
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService,
            router_1.Router,
            dialog_service_1.DialogService])
    ], AuthComponent);
    return AuthComponent;
}());
exports.AuthComponent = AuthComponent;


/***/ }),

/***/ "./src/app/auth/authentication.ts":
/*!****************************************!*\
  !*** ./src/app/auth/authentication.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Authentication = /** @class */ (function () {
    function Authentication() {
        this.userName = "";
        this.token = "";
    }
    return Authentication;
}());
exports.Authentication = Authentication;


/***/ }),

/***/ "./src/app/auth/http-interceptor.module.ts":
/*!*************************************************!*\
  !*** ./src/app/auth/http-interceptor.module.ts ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var auth_service_1 = __webpack_require__(/*! ./auth.service */ "./src/app/auth/auth.service.ts");
var HttpRequestInterceptor = /** @class */ (function () {
    function HttpRequestInterceptor(authService) {
        this.authService = authService;
    }
    HttpRequestInterceptor.prototype.intercept = function (req, next) {
        if (!this.authService.isLoggedIn())
            return next.handle(req);
        var newReq = req.clone({
            headers: req.headers.set('Authorization', 'Bearer ' + this.authService.getToken())
        });
        return next.handle(newReq);
    };
    HttpRequestInterceptor = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [auth_service_1.AuthService])
    ], HttpRequestInterceptor);
    return HttpRequestInterceptor;
}());
exports.HttpRequestInterceptor = HttpRequestInterceptor;
;
var HttpInterceptorModule = /** @class */ (function () {
    function HttpInterceptorModule() {
    }
    HttpInterceptorModule = __decorate([
        core_1.NgModule({
            providers: [{
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: HttpRequestInterceptor,
                    multi: true
                }]
        })
    ], HttpInterceptorModule);
    return HttpInterceptorModule;
}());
exports.HttpInterceptorModule = HttpInterceptorModule;


/***/ }),

/***/ "./src/app/auth/route.guard.ts":
/*!*************************************!*\
  !*** ./src/app/auth/route.guard.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var auth_service_1 = __webpack_require__(/*! ./auth.service */ "./src/app/auth/auth.service.ts");
var RouteGuard = /** @class */ (function () {
    function RouteGuard(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    RouteGuard.prototype.canActivate = function (next, state) {
        if (!this.authService.isLoggedIn()) {
            this.authService.logOut();
            this.router.navigate(['front']);
            return false;
        }
        return true;
    };
    RouteGuard = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [router_1.Router,
            auth_service_1.AuthService])
    ], RouteGuard);
    return RouteGuard;
}());
exports.RouteGuard = RouteGuard;


/***/ }),

/***/ "./src/app/dialogs/dialog.service.ts":
/*!*******************************************!*\
  !*** ./src/app/dialogs/dialog.service.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var ngx_modialog_1 = __webpack_require__(/*! ngx-modialog */ "./node_modules/ngx-modialog/fesm5/ngx-modialog.js");
var bootstrap_1 = __webpack_require__(/*! ngx-modialog/plugins/bootstrap */ "./node_modules/ngx-modialog/plugins/bootstrap/fesm5/ngx-modialog-plugins-bootstrap.js");
var information_dialog_component_1 = __webpack_require__(/*! ./information-dialog/information-dialog.component */ "./src/app/dialogs/information-dialog/information-dialog.component.ts");
var information_dialog_data_1 = __webpack_require__(/*! ./information-dialog/information-dialog-data */ "./src/app/dialogs/information-dialog/information-dialog-data.ts");
var error_report_dialog_component_1 = __webpack_require__(/*! ./error-report-dialog/error-report-dialog.component */ "./src/app/dialogs/error-report-dialog/error-report-dialog.component.ts");
var error_report_data_1 = __webpack_require__(/*! ./error-report-dialog/error-report-data */ "./src/app/dialogs/error-report-dialog/error-report-data.ts");
var registration_form_component_1 = __webpack_require__(/*! ./registration-form/registration-form.component */ "./src/app/dialogs/registration-form/registration-form.component.ts");
var UserModelDto_1 = __webpack_require__(/*! ../UserModelDto */ "./src/app/UserModelDto.ts");
var task_create_dialog_component_1 = __webpack_require__(/*! ./task-create-dialog/task-create-dialog.component */ "./src/app/dialogs/task-create-dialog/task-create-dialog.component.ts");
var task_edit_dialog_component_1 = __webpack_require__(/*! ./task-edit-dialog/task-edit-dialog.component */ "./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.ts");
var TaskDto_1 = __webpack_require__(/*! ../TaskDto */ "./src/app/TaskDto.ts");
var DialogService = /** @class */ (function () {
    function DialogService(modal) {
        this.modal = modal;
    }
    DialogService.prototype.showInformationDialog = function (caption, description) {
        var data = new information_dialog_data_1.InformationDialogData();
        data.caption = caption;
        data.description = description;
        return this.modal.open(information_dialog_component_1.InformationDialogComponent, ngx_modialog_1.overlayConfigFactory(data, bootstrap_1.BSModalContext));
    };
    DialogService.prototype.showErrorReportDialog = function () {
        return this.modal.open(error_report_dialog_component_1.ErrorReportDialogComponent, ngx_modialog_1.overlayConfigFactory(new error_report_data_1.ErrorReportData, bootstrap_1.BSModalContext));
    };
    DialogService.prototype.showRegDialog = function () {
        return this.modal.open(registration_form_component_1.RegistrationFormComponent, ngx_modialog_1.overlayConfigFactory(new UserModelDto_1.UserModelDto, bootstrap_1.BSModalContext));
    };
    DialogService.prototype.showTaskCreateDialog = function () {
        return this.modal.open(task_create_dialog_component_1.TaskCreateDialogComponent, ngx_modialog_1.overlayConfigFactory(new TaskDto_1.TaskDto, bootstrap_1.BSModalContext));
    };
    DialogService.prototype.showTaskEditDialog = function (taskDto) {
        return this.modal.open(task_edit_dialog_component_1.TaskEditDialogComponent, ngx_modialog_1.overlayConfigFactory(taskDto, bootstrap_1.BSModalContext));
    };
    DialogService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [bootstrap_1.Modal])
    ], DialogService);
    return DialogService;
}());
exports.DialogService = DialogService;


/***/ }),

/***/ "./src/app/dialogs/error-report-dialog/error-report-data.ts":
/*!******************************************************************!*\
  !*** ./src/app/dialogs/error-report-dialog/error-report-data.ts ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var bootstrap_1 = __webpack_require__(/*! ngx-modialog/plugins/bootstrap */ "./node_modules/ngx-modialog/plugins/bootstrap/fesm5/ngx-modialog-plugins-bootstrap.js");
var ErrorReportData = /** @class */ (function (_super) {
    __extends(ErrorReportData, _super);
    function ErrorReportData() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ErrorReportData;
}(bootstrap_1.BSModalContext));
exports.ErrorReportData = ErrorReportData;


/***/ }),

/***/ "./src/app/dialogs/error-report-dialog/error-report-dialog.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/dialogs/error-report-dialog/error-report-dialog.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RpYWxvZ3MvZXJyb3ItcmVwb3J0LWRpYWxvZy9lcnJvci1yZXBvcnQtZGlhbG9nLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/dialogs/error-report-dialog/error-report-dialog.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/dialogs/error-report-dialog/error-report-dialog.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row modal-header\">\r\n        <div class=\"col-sm-12\">\r\n            <h3 class=\"modal-title\">Report technical issue</h3>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-body\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <input type=\"text\" placeholder=\"Your name\" class=\"form-control\" [(ngModel)]=\"errorReportData.name\" required />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <input type=\"text\" placeholder=\"Phone number\" class=\"form-control\" [(ngModel)]=\"errorReportData.phoneNumber\" required />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <input type=\"text\" placeholder=\"Email address\" class=\"form-control\" [(ngModel)]=\"errorReportData.email\" required />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <textarea class=\"form-control\" placeholder=\"Describe the problem\" [(ngModel)]=\"errorReportData.description\" rows=\"5\"></textarea>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-footer\">\r\n        <div class=\"col-sm-12\">\r\n            <button type=\"button\" class=\"btn btn-success\" (click)=\"ok()\">\r\n                <fa name=\"check\"></fa>\r\n                Send report\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-danger\" (click)=\"cancel()\">\r\n                <fa name=\"remove\"></fa>\r\n                Cancel\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dialogs/error-report-dialog/error-report-dialog.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/dialogs/error-report-dialog/error-report-dialog.component.ts ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var ngx_modialog_1 = __webpack_require__(/*! ngx-modialog */ "./node_modules/ngx-modialog/fesm5/ngx-modialog.js");
var ErrorReportDialogComponent = /** @class */ (function () {
    function ErrorReportDialogComponent(dialog) {
        this.dialog = dialog;
        //dialog.setCloseGuard(this);
        this.errorReportData = dialog.context;
    }
    ErrorReportDialogComponent.prototype.ngOnInit = function () {
    };
    ErrorReportDialogComponent.prototype.ok = function () {
        this.dialog.close(this.errorReportData);
    };
    ErrorReportDialogComponent.prototype.cancel = function () {
        this.dialog.dismiss();
    };
    ErrorReportDialogComponent.prototype.beforeDismiss = function () {
        return false;
    };
    ErrorReportDialogComponent.prototype.beforeClose = function () {
        return false;
    };
    ErrorReportDialogComponent = __decorate([
        core_1.Component({
            selector: 'app-error-report-dialog',
            template: __webpack_require__(/*! ./error-report-dialog.component.html */ "./src/app/dialogs/error-report-dialog/error-report-dialog.component.html"),
            styles: [__webpack_require__(/*! ./error-report-dialog.component.css */ "./src/app/dialogs/error-report-dialog/error-report-dialog.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_modialog_1.DialogRef])
    ], ErrorReportDialogComponent);
    return ErrorReportDialogComponent;
}());
exports.ErrorReportDialogComponent = ErrorReportDialogComponent;


/***/ }),

/***/ "./src/app/dialogs/information-dialog/information-dialog-data.ts":
/*!***********************************************************************!*\
  !*** ./src/app/dialogs/information-dialog/information-dialog-data.ts ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var bootstrap_1 = __webpack_require__(/*! ngx-modialog/plugins/bootstrap */ "./node_modules/ngx-modialog/plugins/bootstrap/fesm5/ngx-modialog-plugins-bootstrap.js");
var InformationDialogData = /** @class */ (function (_super) {
    __extends(InformationDialogData, _super);
    function InformationDialogData() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return InformationDialogData;
}(bootstrap_1.BSModalContext));
exports.InformationDialogData = InformationDialogData;


/***/ }),

/***/ "./src/app/dialogs/information-dialog/information-dialog.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/dialogs/information-dialog/information-dialog.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RpYWxvZ3MvaW5mb3JtYXRpb24tZGlhbG9nL2luZm9ybWF0aW9uLWRpYWxvZy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/dialogs/information-dialog/information-dialog.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/dialogs/information-dialog/information-dialog.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row modal-header\">\r\n        <div class=\"col-sm-12\">\r\n            <h3 class=\"modal-title\">{{ informationDialogData.caption }}</h3>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-body\">\r\n        <div class=\"col-xs-12\">\r\n            <h4>{{ informationDialogData.description }}</h4>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-footer\">\r\n        <div class=\"col-xs-12\">\r\n            <button type=\"button\" class=\"btn btn-primary\" (click)=\"close()\">\r\n                <fa name=\"remove\"></fa>\r\n                Close\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dialogs/information-dialog/information-dialog.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/dialogs/information-dialog/information-dialog.component.ts ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var ngx_modialog_1 = __webpack_require__(/*! ngx-modialog */ "./node_modules/ngx-modialog/fesm5/ngx-modialog.js");
var InformationDialogComponent = /** @class */ (function () {
    function InformationDialogComponent(dialog) {
        this.dialog = dialog;
        this.informationDialogData = dialog.context;
        dialog.setCloseGuard(this);
    }
    InformationDialogComponent.prototype.ngOnInit = function () {
    };
    InformationDialogComponent.prototype.close = function () {
        this.dialog.close();
    };
    InformationDialogComponent.prototype.beforeDismiss = function () {
        console.log('beforeDismiss');
        return false;
    };
    InformationDialogComponent.prototype.beforeClose = function () {
        console.log('beforeClose');
        return false;
    };
    InformationDialogComponent = __decorate([
        core_1.Component({
            selector: 'app-information-dialog',
            template: __webpack_require__(/*! ./information-dialog.component.html */ "./src/app/dialogs/information-dialog/information-dialog.component.html"),
            styles: [__webpack_require__(/*! ./information-dialog.component.css */ "./src/app/dialogs/information-dialog/information-dialog.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_modialog_1.DialogRef])
    ], InformationDialogComponent);
    return InformationDialogComponent;
}());
exports.InformationDialogComponent = InformationDialogComponent;


/***/ }),

/***/ "./src/app/dialogs/registration-form/registration-form.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/dialogs/registration-form/registration-form.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RpYWxvZ3MvcmVnaXN0cmF0aW9uLWZvcm0vcmVnaXN0cmF0aW9uLWZvcm0uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dialogs/registration-form/registration-form.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/dialogs/registration-form/registration-form.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row modal-header\">\r\n        <div class=\"col-sm-12\">\r\n            <h3 class=\"modal-title\">Register user</h3>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-body\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <input type=\"text\" placeholder=\"Username\" class=\"form-control\" [(ngModel)]=\"registrationData.userId\" name=\"Username\" required />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <input type=\"password\" placeholder=\"Password\" class=\"form-control\" [(ngModel)]=\"registrationData.password\" name=\"Password\" required />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <input type=\"password\" placeholder=\"Confirm password\" class=\"form-control\" [(ngModel)]=\"registrationData.confirmPassword\" name=\"confirmPassword\" required />\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-footer\">\r\n        <div class=\"col-sm-12\">\r\n            <button type=\"button\" class=\"btn btn-success\" (click)=\"ok()\">\r\n                <fa name=\"check\"></fa>\r\n                Register\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-danger\" (click)=\"cancel()\">\r\n                <fa name=\"remove\"></fa>\r\n                Cancel\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dialogs/registration-form/registration-form.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/dialogs/registration-form/registration-form.component.ts ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var ngx_modialog_1 = __webpack_require__(/*! ngx-modialog */ "./node_modules/ngx-modialog/fesm5/ngx-modialog.js");
var RegistrationFormComponent = /** @class */ (function () {
    function RegistrationFormComponent(dialog) {
        this.dialog = dialog;
        dialog.setCloseGuard(this);
        this.registrationData = dialog.context;
    }
    RegistrationFormComponent.prototype.ngOnInit = function () {
    };
    RegistrationFormComponent.prototype.ok = function () {
        this.dialog.close(this.registrationData);
    };
    RegistrationFormComponent.prototype.cancel = function () {
        this.dialog.dismiss();
    };
    RegistrationFormComponent.prototype.beforeDismiss = function () {
        return false;
    };
    RegistrationFormComponent.prototype.beforeClose = function () {
        return false;
    };
    RegistrationFormComponent = __decorate([
        core_1.Component({
            selector: 'app-registration-form',
            template: __webpack_require__(/*! ./registration-form.component.html */ "./src/app/dialogs/registration-form/registration-form.component.html"),
            styles: [__webpack_require__(/*! ./registration-form.component.css */ "./src/app/dialogs/registration-form/registration-form.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_modialog_1.DialogRef])
    ], RegistrationFormComponent);
    return RegistrationFormComponent;
}());
exports.RegistrationFormComponent = RegistrationFormComponent;


/***/ }),

/***/ "./src/app/dialogs/task-create-dialog/task-create-dialog.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/dialogs/task-create-dialog/task-create-dialog.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RpYWxvZ3MvdGFzay1jcmVhdGUtZGlhbG9nL3Rhc2stY3JlYXRlLWRpYWxvZy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/dialogs/task-create-dialog/task-create-dialog.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/dialogs/task-create-dialog/task-create-dialog.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row modal-header\">\r\n        <div class=\"col-sm-12\">\r\n            <h3 class=\"modal-title\">Create task</h3>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-body\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label for=\"taskname\">Name</label>\r\n                <input id=\"taskname\" type=\"text\" placeholder=\"Taskname\" class=\"form-control\" [(ngModel)]=\"taskDto.taskName\" required />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"description\">Description</label>\r\n                <input id=\"description\" type=\"text\" placeholder=\"Description\" class=\"form-control\" [(ngModel)]=\"taskDto.description\" required />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div><label for=\"startDatepicker\">Start date</label></div>\r\n                <ng-datepicker [(ngModel)]=\"startDate\" position=\"top-right\" [options]=\"dpOptions\"></ng-datepicker>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div><label for=\"endDatepicker\">End date</label></div>\r\n                <ng-datepicker [(ngModel)]=\"endDate\" position=\"top-right\" [options]=\"dpOptions\"></ng-datepicker>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-footer\">\r\n        <div class=\"col-sm-12\">\r\n            <button type=\"button\" class=\"btn btn-success\" (click)=\"ok()\">\r\n                <fa name=\"check\"></fa>\r\n                Register\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-danger\" (click)=\"cancel()\">\r\n                <fa name=\"remove\"></fa>\r\n                Cancel\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/dialogs/task-create-dialog/task-create-dialog.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/dialogs/task-create-dialog/task-create-dialog.component.ts ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var ngx_modialog_1 = __webpack_require__(/*! ngx-modialog */ "./node_modules/ngx-modialog/fesm5/ngx-modialog.js");
var svLocale = __webpack_require__(/*! date-fns/locale/sv */ "./node_modules/date-fns/locale/sv/index.js");
var TaskCreateDialogComponent = /** @class */ (function () {
    function TaskCreateDialogComponent(dialog) {
        this.dialog = dialog;
        this.startDate = null;
        this.endDate = null;
        this.dpOptions = {
            minYear: 1970,
            maxYear: 2030,
            displayFormat: 'YYYY-MM-DD',
            barTitleFormat: 'MMMM YYYY',
            dayNamesFormat: 'dd',
            firstCalendarDay: 1,
            locale: svLocale,
            barTitleIfEmpty: 'Välj ett datum',
            placeholder: 'Välj ett datum',
            addClass: 'form-control',
            addStyle: {},
            fieldId: 'my-date-picker',
            useEmptyBarTitle: true,
        };
        this.taskDto = dialog.context;
        dialog.setCloseGuard(this);
    }
    TaskCreateDialogComponent.prototype.ngOnInit = function () { };
    TaskCreateDialogComponent.prototype.ok = function () {
        //this.taskDto.startDate = this.taskDto.startDate + 'T00:00:00';
        //this.taskDto.endDate = this.taskDto.endDate + 'T00:00:00';
        this.taskDto.startDate = this.startDate.toISOString();
        this.taskDto.endDate = this.endDate.toISOString();
        this.dialog.close(this.taskDto);
    };
    TaskCreateDialogComponent.prototype.cancel = function () {
        this.dialog.dismiss();
    };
    TaskCreateDialogComponent.prototype.beforeDismiss = function () {
        return false;
    };
    TaskCreateDialogComponent.prototype.beforeClose = function () {
        return false;
    };
    TaskCreateDialogComponent = __decorate([
        core_1.Component({
            selector: 'app-task-create-dialog',
            template: __webpack_require__(/*! ./task-create-dialog.component.html */ "./src/app/dialogs/task-create-dialog/task-create-dialog.component.html"),
            styles: [__webpack_require__(/*! ./task-create-dialog.component.css */ "./src/app/dialogs/task-create-dialog/task-create-dialog.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_modialog_1.DialogRef])
    ], TaskCreateDialogComponent);
    return TaskCreateDialogComponent;
}());
exports.TaskCreateDialogComponent = TaskCreateDialogComponent;


/***/ }),

/***/ "./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RpYWxvZ3MvdGFzay1lZGl0LWRpYWxvZy90YXNrLWVkaXQtZGlhbG9nLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row modal-header\">\r\n        <div class=\"col-sm-12\">\r\n            <h3 class=\"modal-title\">Edit task</h3>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-body\">\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label for=\"id\">Id</label>\r\n                <input id=\"id\" type=\"text\" placeholder=\"Id\" class=\"form-control\" [(ngModel)]=\"taskDto.id\" disabled=\"disabled\" />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"taskname\">Name</label>\r\n                <input id=\"taskname\" type=\"text\" placeholder=\"Taskname\" class=\"form-control\" [(ngModel)]=\"taskDto.taskName\" required />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"description\">Description</label>\r\n                <input id=\"description\" type=\"text\" placeholder=\"Description\" class=\"form-control\" [(ngModel)]=\"taskDto.description\" required />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div><label for=\"startDatepicker\">Start date</label></div>\r\n                <ng-datepicker [(ngModel)]=\"taskDto.startDate\" position=\"top-right\" [options]=\"dpOptions\"></ng-datepicker>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div><label for=\"endDatepicker\">End date</label></div>\r\n                <ng-datepicker [(ngModel)]=\"taskDto.endDate\" position=\"top-right\" [options]=\"dpOptions\"></ng-datepicker>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row modal-footer\">\r\n        <div class=\"col-sm-12\">\r\n            <button type=\"button\" class=\"btn btn-success\" (click)=\"ok()\">\r\n                <fa name=\"check\"></fa>\r\n                Save\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-danger\" (click)=\"cancel()\">\r\n                <fa name=\"remove\"></fa>\r\n                Cancel\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.ts ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var ngx_modialog_1 = __webpack_require__(/*! ngx-modialog */ "./node_modules/ngx-modialog/fesm5/ngx-modialog.js");
var svLocale = __webpack_require__(/*! date-fns/locale/sv */ "./node_modules/date-fns/locale/sv/index.js");
var TaskEditDialogComponent = /** @class */ (function () {
    function TaskEditDialogComponent(dialog) {
        this.dialog = dialog;
        this.dpOptions = {
            minYear: 1970,
            maxYear: 2030,
            displayFormat: 'YYYY-MM-DD',
            barTitleFormat: 'MMMM YYYY',
            dayNamesFormat: 'dd',
            firstCalendarDay: 1,
            locale: svLocale,
            barTitleIfEmpty: 'Välj ett datum',
            placeholder: 'Välj ett datum',
            addClass: 'form-control',
            addStyle: {},
            fieldId: 'my-date-picker',
            useEmptyBarTitle: true,
        };
        this.taskDto = dialog.context;
        dialog.setCloseGuard(this);
    }
    TaskEditDialogComponent.prototype.ok = function () {
        //this.taskDto.startDate = this.taskDto.startDate + 'T00:00:00';
        //this.taskDto.endDate = this.taskDto.endDate + 'T00:00:00';
        this.dialog.close(this.taskDto);
    };
    TaskEditDialogComponent.prototype.cancel = function () {
        this.dialog.dismiss();
    };
    TaskEditDialogComponent.prototype.beforeDismiss = function () {
        return false;
    };
    TaskEditDialogComponent.prototype.beforeClose = function () {
        return false;
    };
    TaskEditDialogComponent = __decorate([
        core_1.Component({
            selector: 'app-task-edit-dialog',
            template: __webpack_require__(/*! ./task-edit-dialog.component.html */ "./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.html"),
            styles: [__webpack_require__(/*! ./task-edit-dialog.component.css */ "./src/app/dialogs/task-edit-dialog/task-edit-dialog.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_modialog_1.DialogRef])
    ], TaskEditDialogComponent);
    return TaskEditDialogComponent;
}());
exports.TaskEditDialogComponent = TaskEditDialogComponent;


/***/ }),

/***/ "./src/app/front-page/front-page.component.css":
/*!*****************************************************!*\
  !*** ./src/app/front-page/front-page.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zyb250LXBhZ2UvZnJvbnQtcGFnZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/front-page/front-page.component.html":
/*!******************************************************!*\
  !*** ./src/app/front-page/front-page.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"well row\">\r\n    <div class=\"col-md-12\">\r\n        <h1>Getting things done</h1>\r\n        <p>\r\n            Getting Things Done is a time-management method, described in a book of the same title by productivity consultant David Allen. It is often referred to as GTD.\r\n        </p>\r\n        <p>\r\n            The GTD method rests on the idea of moving planned tasks and projects out of the mind by recording them externally and then breaking them into actionable work items. This allows one to focus attention on taking action on tasks, instead of on recalling them.\r\n        </p>\r\n    </div>\r\n\r\n    <div class=\"col-md-4\">\r\n        <h3>Want to learn more</h3>\r\n        <p>\r\n            Check out the inventor of GTD David Allen's book\r\n            <a target=\"_blank\" href=\"http://www.amazon.com/Getting-Things-Done-Stress-Free-Productivity/dp/0143126563/ref=sr_1_2?s=books&ie=UTF8&qid=1431337914&sr=1-2&keywords=Getting+Things+Done%3A+The+Art+of+Stress-Free+Productivity\">\r\n                Getting Things Done: The Art of Stress-Free Productivity\r\n            </a> on\r\n        </p>\r\n        <a target=\"_blank\" href=\"http://www.amazon.com\">\r\n            <img src=\"assets/amazon.png\" alt=\"Amazon.com\">\r\n        </a>\r\n    </div>\r\n\r\n    <div class=\"col-md-4\">\r\n        <h3>Contact</h3>\r\n        For technical issues use the error report form below:\r\n        <button type=\"button\" class=\"btn btn-info\" (click)=\"reportError()\">\r\n            <fa name=\"warning\"></fa>\r\n            Report problem\r\n        </button>\r\n        <p>\r\n            For other issue or press contacts call us at: 073 - 12345678 <br />\r\n            or send an email to: <a href=\"mailto:admin@gtdapp.com\" target=\"_top\">admin@gtdapp.com</a>.\r\n        </p>\r\n    </div>\r\n\r\n    <div class=\"col-md-4\" *ngIf=\"loggedIn()\">\r\n        <h3>New to the Getting things done app</h3>\r\n        <p>Gain control over your time both privately and at work. Register for free and try it out.</p>\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"register()\">\r\n            <fa name=\"user\"></fa>\r\n            Register user\r\n        </button>\r\n    </div>\r\n\r\n    <div class=\"col-md-4\" *ngIf=\"loggedIn()\">\r\n        <h3>Navigation</h3>\r\n        <div style=\"display:inline-block\">\r\n            <a class=\"btn btn-info\" role=\"button\" routerLink=\"/taskMgmt\" routerLinkActive=\"active\">\r\n                <fa name=\"edit\"></fa>\r\n                Main page\r\n            </a>\r\n        </div>\r\n        <div style=\"display:inline-block\">\r\n            <a class=\"btn btn-info\" role=\"button\" routerLink=\"/stateMgmt\" routerLinkActive=\"active\">\r\n                <fa name=\"list\"></fa>\r\n                Manage your tasks\r\n            </a>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/front-page/front-page.component.ts":
/*!****************************************************!*\
  !*** ./src/app/front-page/front-page.component.ts ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
//import { Overlay } from 'ngx-modialog';
//import { Modal } from 'ngx-modialog/plugins/bootstrap';
var auth_service_1 = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
var dialog_service_1 = __webpack_require__(/*! ../dialogs/dialog.service */ "./src/app/dialogs/dialog.service.ts");
var FrontPageComponent = /** @class */ (function () {
    function FrontPageComponent(authService, dialogService) {
        this.authService = authService;
        this.dialogService = dialogService;
    }
    FrontPageComponent.prototype.ngOnInit = function () {
    };
    FrontPageComponent.prototype.loggedIn = function () {
        return this.authService.isLoggedIn();
    };
    FrontPageComponent.prototype.register = function () {
        var _this = this;
        this.dialogService.showRegDialog()
            .result.then(function (returnData) {
            _this.authService.createUser(returnData)
                .subscribe(function (res) {
                _this.dialogService.showInformationDialog('Welcome to the Gtd community', 'Thank you for joining our website, we hope youe life will become more efficient and productive with our help. An email with a confirmation link has been sent to the email address you supplied, please click the link to activate your registration before trying to login.');
            }), function (err) {
                _this.dialogService.showInformationDialog('Registration failed', 'Error message: ' + err);
            };
        });
    };
    FrontPageComponent.prototype.reportError = function () {
        this.dialogService.showErrorReportDialog()
            .result.then(function (dialog) {
            dialog.result.then(function (returnData) {
                //anropa errorreportservice (finns inte �n)
                //alert(
                //	'name: ' + returnData.name + '\n' +
                //	'phoneNumber: ' + returnData.phoneNumber + '\n' +
                //	'email: ' + returnData.email + '\n' +
                //	'description: ' + returnData.description);
            });
        });
    };
    FrontPageComponent = __decorate([
        core_1.Component({
            selector: 'app-front-page',
            template: __webpack_require__(/*! ./front-page.component.html */ "./src/app/front-page/front-page.component.html"),
            styles: [__webpack_require__(/*! ./front-page.component.css */ "./src/app/front-page/front-page.component.css")]
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService,
            dialog_service_1.DialogService])
    ], FrontPageComponent);
    return FrontPageComponent;
}());
exports.FrontPageComponent = FrontPageComponent;


/***/ }),

/***/ "./src/app/globals.ts":
/*!****************************!*\
  !*** ./src/app/globals.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Globals = Object.freeze({
    AUTH_KEY: 'Auth',
    //BASE_API_URL: window.location.origin + '/api'
    BASE_API_URL: 'http://localhost:50454' + '/api'
});


/***/ }),

/***/ "./src/app/state-mgmt/state-filter.pipe.ts":
/*!*************************************************!*\
  !*** ./src/app/state-mgmt/state-filter.pipe.ts ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var StateFilterPipe = /** @class */ (function () {
    function StateFilterPipe() {
    }
    StateFilterPipe.prototype.transform = function (items, arg) {
        return items.filter(function (item) { return item.state == arg; });
    };
    StateFilterPipe = __decorate([
        core_1.Pipe({
            name: 'stateFilter',
            pure: false
        })
    ], StateFilterPipe);
    return StateFilterPipe;
}());
exports.StateFilterPipe = StateFilterPipe;


/***/ }),

/***/ "./src/app/state-mgmt/state-mgmt.component.css":
/*!*****************************************************!*\
  !*** ./src/app/state-mgmt/state-mgmt.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".swimLane {\r\n\tmargin-bottom: 1px;\r\n\tdisplay: flex;\r\n\tflex-flow: row nowrap;\r\n\talign-content: flex-start;\r\n\talign-items: stretch;\r\n\tfont-family: \"Helvetica Neue\",Helvetica,Arial,sans-serif;\r\n\tline-height: 1.42857143;\r\n}\r\n\r\n.task {\r\n\torder: 2;\r\n\tflex: 1 1 auto;\r\n\tborder-top: 1px solid black;\r\n\tborder-bottom: 1px solid black;\r\n}\r\n\r\n.leftMostTask {\r\n\tborder-left: 1px solid black;\r\n}\r\n\r\n.rightMostTask {\r\n\tborder-right: 1px solid black;\r\n}\r\n\r\n.taskUpper {\r\n\tbackground-color: #FFFFFF;\r\n\tfont-size: 18px;\r\n\tpadding: 10px;\r\n}\r\n\r\n.taskLower {\r\n\tbackground-color: #F5F5F5;\r\n\tborder-top: 1px solid #DDDDDD;\r\n\tfont-size: 14px;\r\n\tpadding: 10px;\r\n}\r\n\r\n.stateButton {\r\n\talign-self: stretch;\r\n\talign-items: center;\r\n\tdisplay: flex;\r\n\tmin-width: 1.2em;\r\n\tbackground-color: white;\r\n\tborder: 1px solid black;\r\n}\r\n\r\n.leftCorner {\r\n\tborder-top-left-radius: 5px;\r\n\tborder-bottom-left-radius: 5px;\r\n\toverflow: hidden;\r\n\torder: 1;\r\n}\r\n\r\n.rightCorner {\r\n\tborder-top-right-radius: 5px;\r\n\tborder-bottom-right-radius: 5px;\r\n\toverflow: hidden;\r\n\torder: 3;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3RhdGUtbWdtdC9zdGF0ZS1tZ210LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Q0FDQyxtQkFBbUI7Q0FDbkIsY0FBYztDQUNkLHNCQUFzQjtDQUN0QiwwQkFBMEI7Q0FDMUIscUJBQXFCO0NBQ3JCLHlEQUF5RDtDQUN6RCx3QkFBd0I7Q0FDeEI7O0FBRUQ7Q0FDQyxTQUFTO0NBQ1QsZUFBZTtDQUNmLDRCQUE0QjtDQUM1QiwrQkFBK0I7Q0FDL0I7O0FBRUQ7Q0FDQyw2QkFBNkI7Q0FDN0I7O0FBRUQ7Q0FDQyw4QkFBOEI7Q0FDOUI7O0FBRUQ7Q0FDQywwQkFBMEI7Q0FDMUIsZ0JBQWdCO0NBQ2hCLGNBQWM7Q0FDZDs7QUFFRDtDQUNDLDBCQUEwQjtDQUMxQiw4QkFBOEI7Q0FDOUIsZ0JBQWdCO0NBQ2hCLGNBQWM7Q0FDZDs7QUFFRDtDQUNDLG9CQUFvQjtDQUNwQixvQkFBb0I7Q0FDcEIsY0FBYztDQUNkLGlCQUFpQjtDQUNqQix3QkFBd0I7Q0FDeEIsd0JBQXdCO0NBQ3hCOztBQUVEO0NBQ0MsNEJBQTRCO0NBQzVCLCtCQUErQjtDQUMvQixpQkFBaUI7Q0FDakIsU0FBUztDQUNUOztBQUVEO0NBQ0MsNkJBQTZCO0NBQzdCLGdDQUFnQztDQUNoQyxpQkFBaUI7Q0FDakIsU0FBUztDQUNUIiwiZmlsZSI6InNyYy9hcHAvc3RhdGUtbWdtdC9zdGF0ZS1tZ210LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3dpbUxhbmUge1xyXG5cdG1hcmdpbi1ib3R0b206IDFweDtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGZsZXgtZmxvdzogcm93IG5vd3JhcDtcclxuXHRhbGlnbi1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG5cdGFsaWduLWl0ZW1zOiBzdHJldGNoO1xyXG5cdGZvbnQtZmFtaWx5OiBcIkhlbHZldGljYSBOZXVlXCIsSGVsdmV0aWNhLEFyaWFsLHNhbnMtc2VyaWY7XHJcblx0bGluZS1oZWlnaHQ6IDEuNDI4NTcxNDM7XHJcbn1cclxuXHJcbi50YXNrIHtcclxuXHRvcmRlcjogMjtcclxuXHRmbGV4OiAxIDEgYXV0bztcclxuXHRib3JkZXItdG9wOiAxcHggc29saWQgYmxhY2s7XHJcblx0Ym9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGJsYWNrO1xyXG59XHJcblxyXG4ubGVmdE1vc3RUYXNrIHtcclxuXHRib3JkZXItbGVmdDogMXB4IHNvbGlkIGJsYWNrO1xyXG59XHJcblxyXG4ucmlnaHRNb3N0VGFzayB7XHJcblx0Ym9yZGVyLXJpZ2h0OiAxcHggc29saWQgYmxhY2s7XHJcbn1cclxuXHJcbi50YXNrVXBwZXIge1xyXG5cdGJhY2tncm91bmQtY29sb3I6ICNGRkZGRkY7XHJcblx0Zm9udC1zaXplOiAxOHB4O1xyXG5cdHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi50YXNrTG93ZXIge1xyXG5cdGJhY2tncm91bmQtY29sb3I6ICNGNUY1RjU7XHJcblx0Ym9yZGVyLXRvcDogMXB4IHNvbGlkICNEREREREQ7XHJcblx0Zm9udC1zaXplOiAxNHB4O1xyXG5cdHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5zdGF0ZUJ1dHRvbiB7XHJcblx0YWxpZ24tc2VsZjogc3RyZXRjaDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0bWluLXdpZHRoOiAxLjJlbTtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuXHRib3JkZXI6IDFweCBzb2xpZCBibGFjaztcclxufVxyXG5cclxuLmxlZnRDb3JuZXIge1xyXG5cdGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDVweDtcclxuXHRib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA1cHg7XHJcblx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRvcmRlcjogMTtcclxufVxyXG5cclxuLnJpZ2h0Q29ybmVyIHtcclxuXHRib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNXB4O1xyXG5cdGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA1cHg7XHJcblx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRvcmRlcjogMztcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/state-mgmt/state-mgmt.component.html":
/*!******************************************************!*\
  !*** ./src/app/state-mgmt/state-mgmt.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"well\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-3\">\r\n            <h3>New</h3>\r\n            <div id=\"newTasks\" *ngFor=\"let task of tasks | stateFilter:'New'\" class=\"swimLane\">\r\n                <div class=\"task leftCorner leftMostTask\">\r\n                    <div class=\"taskUpper\">\r\n                        {{task.taskName}}\r\n                    </div>\r\n                    <div class=\"taskLower\">\r\n                        {{task.description}}\r\n                    </div>\r\n                </div>\r\n                <div (click)=\"moveToNextState(task.id)\" class=\"stateButton rightCorner\">\r\n                    <fa name=\"chevron-right\"></fa>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-3\">\r\n            <h3>Analyzed</h3>\r\n            <div id=\"analyzedTasks\" *ngFor=\"let task of tasks | stateFilter:'Analyzed'\" class=\"swimLane\">\r\n                <div (click)=\"moveToPreviousState(task.id)\" class=\"stateButton leftCorner\">\r\n                    <fa name=\"chevron-left\"></fa>\r\n                </div>\r\n                <div class=\"task\">\r\n                    <div class=\"taskUpper\">\r\n                        {{task.taskName}}\r\n                    </div>\r\n                    <div class=\"taskLower\">\r\n                        {{task.description}}\r\n                    </div>\r\n                </div>\r\n                <div (click)=\"moveToNextState(task.id)\" class=\"stateButton rightCorner\">\r\n                    <fa name=\"chevron-right\"></fa>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-3\">\r\n            <h3>In progress</h3>\r\n            <div id=\"inProgressTasks\" *ngFor=\"let task of tasks | stateFilter:'In progress'\" class=\"swimLane\">\r\n                <div (click)=\"moveToPreviousState(task.id)\" class=\"stateButton leftCorner\">\r\n                    <fa name=\"chevron-left\"></fa>\r\n                </div>\r\n                <div class=\"task\">\r\n                    <div class=\"taskUpper\">\r\n                        {{task.taskName}}\r\n                    </div>\r\n                    <div class=\"taskLower\">\r\n                        {{task.description}}\r\n                    </div>\r\n                </div>\r\n                <div (click)=\"moveToNextState(task.id)\" class=\"stateButton rightCorner\">\r\n                    <fa name=\"chevron-right\"></fa>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-md-3\">\r\n            <h3>Finished</h3>\r\n            <div id=\"finishedTasks\" *ngFor=\"let task of tasks | stateFilter:'Finished'\" class=\"swimLane\">\r\n                <div (click)=\"moveToPreviousState(task.id)\" class=\"stateButton leftCorner\">\r\n                    <fa name=\"chevron-left\"></fa>\r\n                </div>\r\n                <div class=\"task rightCorner rightMostTask\">\r\n                    <div class=\"taskUpper\">\r\n                        {{task.taskName}}\r\n                    </div>\r\n                    <div class=\"taskLower\">\r\n                        {{task.description}}\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <p style=\"margin-top: 10px\">\r\n        <a class=\"btn btn-info\" role=\"button\" routerLink=\"/taskMgmt\" routerLinkActive=\"active\">\r\n            Main page\r\n        </a>\r\n    </p>\r\n</div>"

/***/ }),

/***/ "./src/app/state-mgmt/state-mgmt.component.ts":
/*!****************************************************!*\
  !*** ./src/app/state-mgmt/state-mgmt.component.ts ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var task_service_1 = __webpack_require__(/*! ../task-mgmt/task.service */ "./src/app/task-mgmt/task.service.ts");
var dialog_service_1 = __webpack_require__(/*! ../dialogs/dialog.service */ "./src/app/dialogs/dialog.service.ts");
var MoveTaskDto_1 = __webpack_require__(/*! ../MoveTaskDto */ "./src/app/MoveTaskDto.ts");
var StateMgmtComponent = /** @class */ (function () {
    function StateMgmtComponent(taskService, dialogService) {
        this.taskService = taskService;
        this.dialogService = dialogService;
    }
    StateMgmtComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.taskService.initTasks()
            .subscribe(function (success) { }, function (error) { return _this.dialogService.showInformationDialog("Failed to fetch tasks", error); });
        this.tasks = this.taskService.getTasks();
    };
    StateMgmtComponent.prototype.moveToNextState = function (taskId) {
        var _this = this;
        var moveTaskDto = new MoveTaskDto_1.MoveTaskDto();
        moveTaskDto.taskId = taskId;
        moveTaskDto.direction = 1;
        this.taskService.moveTask(moveTaskDto)
            .subscribe(function (success) { }, function (error) { return _this.dialogService.showInformationDialog("Failed to move task forward", error); });
    };
    StateMgmtComponent.prototype.moveToPreviousState = function (taskId) {
        var _this = this;
        var moveTaskDto = new MoveTaskDto_1.MoveTaskDto();
        moveTaskDto.taskId = taskId;
        moveTaskDto.direction = 0;
        this.taskService.moveTask(moveTaskDto)
            .subscribe(function (success) { }, function (error) { return _this.dialogService.showInformationDialog("Failed to move task backwards", error); });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], StateMgmtComponent.prototype, "tasks", void 0);
    StateMgmtComponent = __decorate([
        core_1.Component({
            selector: 'app-state-mgmt',
            template: __webpack_require__(/*! ./state-mgmt.component.html */ "./src/app/state-mgmt/state-mgmt.component.html"),
            styles: [__webpack_require__(/*! ./state-mgmt.component.css */ "./src/app/state-mgmt/state-mgmt.component.css")]
        }),
        __metadata("design:paramtypes", [task_service_1.TaskService,
            dialog_service_1.DialogService])
    ], StateMgmtComponent);
    return StateMgmtComponent;
}());
exports.StateMgmtComponent = StateMgmtComponent;


/***/ }),

/***/ "./src/app/task-mgmt/task-mgmt.component.css":
/*!***************************************************!*\
  !*** ./src/app/task-mgmt/task-mgmt.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tralign {\r\n\tvertical-align: middle;\r\n}\r\n\r\n.invsible {\r\n\tdisplay: none\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFzay1tZ210L3Rhc2stbWdtdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0NBQ0MsdUJBQXVCO0NBQ3ZCOztBQUVEO0NBQ0MsYUFBYTtDQUNiIiwiZmlsZSI6InNyYy9hcHAvdGFzay1tZ210L3Rhc2stbWdtdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRyYWxpZ24ge1xyXG5cdHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbi5pbnZzaWJsZSB7XHJcblx0ZGlzcGxheTogbm9uZVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/task-mgmt/task-mgmt.component.html":
/*!****************************************************!*\
  !*** ./src/app/task-mgmt/task-mgmt.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Your tasks</h1>\r\n<table class=\"table table-striped table-bordered\">\r\n    <thead>\r\n        <tr>\r\n            <th class=\"invsible\">Id</th>\r\n            <th>Name</th>\r\n            <th>Description</th>\r\n            <th>State</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n        <tr *ngFor=\"let task of tasks\">\r\n            <td class=\"invsible\">{{ task.id }}</td>\r\n            <td style=\"vertical-align: middle;\">{{ task.taskName }}</td>\r\n            <td style=\"vertical-align: middle;\">{{ task.description }}</td>\r\n            <td style=\"vertical-align: middle;\">{{ task.state }}</td>\r\n            <td>\r\n                <button class=\"btn btn-info\" (click)=\"editTask(task.id)\">\r\n                    <fa name=\"edit\"></fa>\r\n                    Edit\r\n                </button>\r\n            </td>\r\n            <td>\r\n                <button class=\"btn btn-info\" (click)=\"deleteTask(task.id)\">\r\n                    <fa name=\"trash\"></fa>                    \r\n                    Delete\r\n                </button>\r\n            </td>\r\n        </tr>\r\n            \r\n    </tbody>\r\n</table>\r\n<button type=\"button\" class=\"btn btn-info\" (click)=\"createTask()\">\r\n    <fa name=\"plus-circle\"></fa>\r\n    Create task\r\n</button>\r\n<a class=\"btn btn-info\" role=\"button\" routerLink=\"/stateMgmt\" routerLinkActive=\"active\">\r\n    <fa name=\"list\"></fa>\r\n    Manage your tasks\r\n</a>\r\n"

/***/ }),

/***/ "./src/app/task-mgmt/task-mgmt.component.ts":
/*!**************************************************!*\
  !*** ./src/app/task-mgmt/task-mgmt.component.ts ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var dialog_service_1 = __webpack_require__(/*! ../dialogs/dialog.service */ "./src/app/dialogs/dialog.service.ts");
var task_service_1 = __webpack_require__(/*! ./task.service */ "./src/app/task-mgmt/task.service.ts");
var TaskMgmtComponent = /** @class */ (function () {
    function TaskMgmtComponent(taskService, dialogService) {
        this.taskService = taskService;
        this.dialogService = dialogService;
    }
    TaskMgmtComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.taskService.initTasks()
            .subscribe(function (success) { }, function (error) { return _this.dialogService.showInformationDialog("Failed to fetch tasks", error); });
        this.tasks = this.taskService.getTasks();
    };
    TaskMgmtComponent.prototype.createTask = function () {
        var _this = this;
        this.dialogService.showTaskCreateDialog()
            .result.then(function (taskDto) {
            _this.taskService.createTask(taskDto)
                .subscribe(function (success) { }, function (error) { return _this.dialogService.showInformationDialog("Failed to create task", error); });
        }, function () { });
    };
    TaskMgmtComponent.prototype.editTask = function (taskId) {
        var _this = this;
        var selectedTask = this.tasks.find(function (t) { return t.id == taskId; });
        this.dialogService.showTaskEditDialog(selectedTask)
            .result.then(function (taskDto) {
            _this.taskService.editTask(taskDto)
                .subscribe(function (success) { }, function (error) { return _this.dialogService.showInformationDialog("Failed to edit task", error); });
        }, function () { });
    };
    TaskMgmtComponent.prototype.deleteTask = function (taskId) {
        var _this = this;
        this.taskService.deleteTask(taskId)
            .subscribe(function (success) { }, function (error) { return _this.dialogService.showInformationDialog("Failed to delete task", error); });
    };
    TaskMgmtComponent = __decorate([
        core_1.Component({
            selector: 'task-mgmt',
            template: __webpack_require__(/*! ./task-mgmt.component.html */ "./src/app/task-mgmt/task-mgmt.component.html"),
            styles: [__webpack_require__(/*! ./task-mgmt.component.css */ "./src/app/task-mgmt/task-mgmt.component.css")]
        }),
        __metadata("design:paramtypes", [task_service_1.TaskService,
            dialog_service_1.DialogService])
    ], TaskMgmtComponent);
    return TaskMgmtComponent;
}());
exports.TaskMgmtComponent = TaskMgmtComponent;


/***/ }),

/***/ "./src/app/task-mgmt/task.service.ts":
/*!*******************************************!*\
  !*** ./src/app/task-mgmt/task.service.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var rxjs_1 = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var operators_1 = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var ngx_webstorage_1 = __webpack_require__(/*! ngx-webstorage */ "./node_modules/ngx-webstorage/fesm5/ngx-webstorage.js");
var TaskDto_1 = __webpack_require__(/*! ../TaskDto */ "./src/app/TaskDto.ts");
var globals_1 = __webpack_require__(/*! ../globals */ "./src/app/globals.ts");
var TaskService = /** @class */ (function () {
    function TaskService(http, localStorageService) {
        this.http = http;
        this.localStorageService = localStorageService;
        this.taskUrl = globals_1.Globals.BASE_API_URL + '/Task';
        this.tasks = new Array();
        var authentication = this.localStorageService.retrieve(globals_1.Globals.AUTH_KEY);
        this.headers = new http_1.HttpHeaders()
            .set('Content-Type', 'application/json');
        this.userName = authentication.userName;
    }
    TaskService.prototype.initTasks = function () {
        var _this = this;
        var params = new http_1.HttpParams()
            .set('userName', this.userName);
        return this.http.get(this.taskUrl, {
            params: params,
            headers: this.headers,
            responseType: 'json'
        })
            .pipe(operators_1.tap(function (data) {
            if (data) {
                _this.tasks.length = 0;
                for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                    var elem = data_1[_i];
                    _this.tasks.push(new TaskDto_1.TaskDto(elem));
                }
            }
        }), operators_1.catchError(this.handleError("GET: " + this.taskUrl)));
    };
    TaskService.prototype.getTasks = function () {
        return this.tasks;
    };
    TaskService.prototype.editTask = function (taskDto) {
        var _this = this;
        return this.http.put(this.taskUrl, taskDto, {
            headers: this.headers,
            responseType: 'json'
        })
            .pipe(operators_1.tap(function (updatedTask) {
            var taskIndex = _this.tasks.findIndex(function (t) { return t.id == updatedTask.id; });
            _this.tasks[taskIndex] = updatedTask;
        }), operators_1.catchError(this.handleError("PUT: " + this.taskUrl)));
    };
    TaskService.prototype.createTask = function (taskDto) {
        var _this = this;
        taskDto.userName = this.userName;
        return this.http.post(this.taskUrl, taskDto, {
            headers: this.headers,
            responseType: 'json'
        })
            .pipe(operators_1.tap(function (newTask) { return _this.tasks.push(newTask); }), operators_1.catchError(this.handleError("POST: " + this.taskUrl)));
    };
    TaskService.prototype.deleteTask = function (taskId) {
        var _this = this;
        var params = new http_1.HttpParams()
            .set('taskId', String(taskId));
        return this.http.delete(this.taskUrl, {
            params: params,
            headers: this.headers,
            responseType: 'json'
        })
            .pipe(operators_1.tap(function (retVal) {
            var taskIndex = _this.tasks.findIndex(function (t) { return t.id == retVal; });
            _this.tasks.splice(taskIndex, 1);
        }), operators_1.catchError(this.handleError("DELETE: " + this.taskUrl)));
    };
    TaskService.prototype.moveTask = function (moveTaskDto) {
        var _this = this;
        return this.http.patch(this.taskUrl, moveTaskDto, {
            headers: this.headers,
            responseType: 'json'
        })
            .pipe(operators_1.tap(function (taskDto) {
            var taskIndex = _this.tasks.findIndex(function (t) { return t.id == taskDto.id; });
            _this.tasks[taskIndex].state = taskDto.state;
        }), operators_1.catchError(this.handleError("PATCH: " + this.taskUrl)));
    };
    TaskService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            //TODO: send the error to remote logging infrastructure
            console.log(error); // log to console instead
            //throw an error to run the error function in subscribe
            return rxjs_1.throwError(error.error);
        };
    };
    TaskService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient,
            ngx_webstorage_1.LocalStorageService])
    ], TaskService);
    return TaskService;
}());
exports.TaskService = TaskService;


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
var environment_1 = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Dokument\Dev\CS\gtdcore\GtdCore.Web\gtd-client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map