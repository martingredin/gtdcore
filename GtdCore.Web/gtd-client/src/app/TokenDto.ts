﻿

// Created with Typewriter (http://frhagn.github.io/Typewriter/)


export class TokenDto {
    
    // TOKEN
    public token: string = null;
    // EXPIRESIN
    public expiresIn: Date = new Date(0);

    constructor(data: any = null) {
        if (data !== null) {
            this.token = data.token;
            this.expiresIn = new Date(data.expiresIn);
        }
    }
}
