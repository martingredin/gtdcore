import { Component, OnInit } from '@angular/core';
import { DialogService } from '../dialogs/dialog.service';
import { TaskService } from './task.service';
import { TaskDto } from '../TaskDto';

@Component({
	selector: 'task-mgmt',
	templateUrl: './task-mgmt.component.html',
	styleUrls: ['./task-mgmt.component.css']
})
export class TaskMgmtComponent implements OnInit {
	private tasks: Array<TaskDto>;

	constructor(
		private taskService: TaskService,
		private dialogService: DialogService)
	{ }

	ngOnInit() {
		this.taskService.initTasks()
			.subscribe(
				success => { },
				error => this.dialogService.showInformationDialog("Failed to fetch tasks", error)
			);
		this.tasks = this.taskService.getTasks();
	}

	createTask() {
		this.dialogService.showTaskCreateDialog()
			.result.then(
			taskDto => {
				this.taskService.createTask(taskDto)
					.subscribe(
						success => { },
						error => this.dialogService.showInformationDialog("Failed to create task", error)
					);
			},

			() => { });
	}

	editTask(taskId: number) {
		let selectedTask = this.tasks.find(t => t.id == taskId);
		this.dialogService.showTaskEditDialog(selectedTask)
			.result.then(
			taskDto => {
				this.taskService.editTask(taskDto)
					.subscribe(
						success => { },
						error => this.dialogService.showInformationDialog("Failed to edit task", error)
				);

			},
			() => { });
	}

	deleteTask(taskId: number) {
		this.taskService.deleteTask(taskId)
			.subscribe(
				success => { },
				error => this.dialogService.showInformationDialog("Failed to delete task", error)
			);
	}
}
