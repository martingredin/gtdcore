import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { LocalStorageService } from 'ngx-webstorage';
import { TaskDto } from '../TaskDto';
import { MoveTaskDto } from '../MoveTaskDto';
import { Globals } from '../globals';
import { Authentication } from '../auth/authentication';

@Injectable({
	providedIn: 'root'
})
export class TaskService {
	private readonly taskUrl = Globals.BASE_API_URL + '/Task';
	private headers;
	private tasks: Array<TaskDto> = new Array<TaskDto>();
	private userName;

	constructor(
		private http: HttpClient,
		private localStorageService: LocalStorageService)
	{
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;
		this.headers = new HttpHeaders()
			.set('Content-Type', 'application/json');
		this.userName = authentication.userName;
	}

	public initTasks() {
		const params = new HttpParams()
			.set('userName', this.userName);

		return this.http.get(this.taskUrl,
			{
				params: params,
				headers: this.headers,
				responseType: 'json'
			})
			.pipe(
				tap((data: Array<TaskDto>) => {
					if (data) {
						this.tasks.length = 0;
						for (let elem of data)
							this.tasks.push(new TaskDto(elem));
					}
				}),
				catchError(this.handleError<string>("GET: " + this.taskUrl))
			);
	}

	getTasks(): Array<TaskDto> {
		return this.tasks;
	}

	editTask(taskDto: TaskDto) {
		return this.http.put(this.taskUrl, taskDto, {
			headers: this.headers,
			responseType: 'json'
		})
		.pipe(
			tap((updatedTask: TaskDto) => {
				let taskIndex = this.tasks.findIndex(t => t.id == updatedTask.id);
				this.tasks[taskIndex] = updatedTask;
			}),
			catchError(this.handleError<string>("PUT: " + this.taskUrl))
		);
	}

	createTask(taskDto: TaskDto) {
		taskDto.userName = this.userName;

		return this.http.post(this.taskUrl, taskDto, {
			headers: this.headers,
			responseType: 'json'
		})
		.pipe(
			tap((newTask: TaskDto) => this.tasks.push(newTask)),
			catchError(this.handleError<string>("POST: " + this.taskUrl))
		);
	}

	deleteTask(taskId: number) {
		const params = new HttpParams()
			.set('taskId', String(taskId));

		return this.http.delete(this.taskUrl, {
				params: params,
				headers: this.headers,
				responseType: 'json'
		})
		.pipe(
			tap((retVal: number) => {
				let taskIndex = this.tasks.findIndex(t => t.id == retVal);
				this.tasks.splice(taskIndex, 1);
			}),
			catchError(this.handleError<string>("DELETE: " + this.taskUrl))
		);
	}

	moveTask(moveTaskDto: MoveTaskDto) {
		return this.http.patch(this.taskUrl, moveTaskDto, {
			headers: this.headers,
			responseType: 'json'
		})
		.pipe(
			tap((taskDto: TaskDto) => {
				let taskIndex = this.tasks.findIndex(t => t.id == taskDto.id);
				this.tasks[taskIndex].state = taskDto.state;
			}),
			catchError(this.handleError<string>("PATCH: " + this.taskUrl))
		);
	}

	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {

			//TODO: send the error to remote logging infrastructure
			console.log(error); // log to console instead

			//throw an error to run the error function in subscribe
			return throwError(error.error);
		};
	}
}