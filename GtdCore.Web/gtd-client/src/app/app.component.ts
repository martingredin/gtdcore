import { Component } from '@angular/core';
import { Overlay } from 'ngx-modialog';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import { DialogService } from './dialogs/dialog.service';
import { AuthService } from './auth/auth.service';
import { TaskService } from './task-mgmt/task.service';

import { TaskDto } from './TaskDto';
import { LoginDto } from './LoginDto';
import { UserModelDto } from './UserModelDto';



@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'gtd-client';
    constructor(
        public modal: Modal,
        public dialogService: DialogService,
		public authService: AuthService) { }

    showDlg() {
        const dialog =
            this.modal.confirm()
                .okBtn("Yes")
                .cancelBtn("No")
                .size('sm')
                .showClose(true)
                .title('Confirm')
                .body("Do you want to accept?")
                .open();

        dialog.result.then(res => { console.log("res: " + res) }, err => { console.log("rejected") });
    }

    showInfoDlg() {
        this.dialogService
            .showInformationDialog('info heading', 'info body')
            .result.then(
                res => console.log("ok:"),
                err => console.log("cancel: " + err));
    }

    showErrorRepDlg() {
        this.dialogService
			.showErrorReportDialog()
            .result.then(
				res => console.log("ok:\n" + res.name + '\n' + res.phoneNumber + '\n' + res.email + '\n' + res.description),
                err => console.log("cancel: " + err));
    }

    showRegForm() {
        this.dialogService
            .showRegDialog()
            .result.then(
                res => console.log("ok:\n" + res.userId + '\n' + res.password + '\n' + res.confirmPassword),
				err => console.log("rejected: " + err));
    }

    showTCDlg() {
        this.dialogService
            .showTaskCreateDialog()
            .result.then(
				res => console.log("ok:\n" + res.taskName + '\n' + res.description + '\n' + res.startDate + '\n' + res.endDate),
				err => console.log("rejected: " + err));
    }

    showTEDlg() {
        let taskDto = new TaskDto();
        taskDto.id = 55;
        taskDto.taskName = 'afsaefsdf';
        taskDto.userName = 'asdfcasfcs';
        taskDto.description = 'vdfhgyhty';
        taskDto.startDate = '2018-10-10';
        taskDto.endDate = '2019-01-01';
        

        this.dialogService
            .showTaskEditDialog(taskDto)
            .result.then(
                res => console.log("ok:\n" + res.taskName + '\n' + res.description + '\n' + res.startDate + '\n' + res.endDate),
                err => console.log("rejected: " + err));
    }

    logIn() {
        console.log("entering login");

		this.authService.login("Olle", "fgh321")
            .subscribe(res => {
                console.log('success: ' + res);
            }), err => {
                console.log("Error Occured " + err);
            }
	}

	isLoggedIn() {
		if (this.authService.isLoggedIn()) {
			var token = this.authService.getToken();
			this.dialogService.showInformationDialog('isLoggedIn s�ger att', 'vi �r inloggade med token: ' + token);
		}
		else
			this.dialogService.showInformationDialog('isLoggedIn s�ger att', 'vi inte �r inloggade');
	}

	logOut() {
		this.authService.logOut();
		this.dialogService.showInformationDialog('', 'du �r nu utloggad');
	}

	register() {
		this.dialogService.showRegDialog()
			.result.then(returnData => {
				this.authService.createUser(returnData)
					.subscribe(res => {
						this.dialogService.showInformationDialog(
							'Welcome to the Gtd community',
							'Thank you for joining our website, we hope youe life will become more efficient and productive with our help. An email with a confirmation link has been sent to the email address you supplied, please click the link to activate your registration before trying to login.')
					}), err => {
						this.dialogService.showInformationDialog(
							'Registration failed',
							'Error message: ' + err)
					}
			});
	}


}