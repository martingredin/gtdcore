import { Pipe, PipeTransform } from '@angular/core';
import { TaskDto } from '../TaskDto';

@Pipe({
	name: 'stateFilter',
	pure: false
})
export class StateFilterPipe implements PipeTransform {
	transform(items: TaskDto[], arg: string): any {
		return items.filter(item => item.state == arg);
	}
}
