import { Component, OnInit, Input } from '@angular/core';
import { TaskService } from '../task-mgmt/task.service';
import { DialogService } from '../dialogs/dialog.service';
import { TaskDto } from '../TaskDto';
import { MoveTaskDto } from '../MoveTaskDto';

@Component({
	selector: 'app-state-mgmt',
	templateUrl: './state-mgmt.component.html',
	styleUrls: ['./state-mgmt.component.css']
})
export class StateMgmtComponent implements OnInit {
	@Input() private tasks: Array<TaskDto>;

	constructor(
		private taskService: TaskService,
		private dialogService: DialogService) { }

	ngOnInit() {
		this.taskService.initTasks()
			.subscribe(
				success => { },
				error => this.dialogService.showInformationDialog("Failed to fetch tasks", error)
			);
		this.tasks = this.taskService.getTasks();
	}

	moveToNextState(taskId: number) {
		let moveTaskDto = new MoveTaskDto();
		moveTaskDto.taskId = taskId;
		moveTaskDto.direction = 1;
		this.taskService.moveTask(moveTaskDto)
			.subscribe(
				success => { },
				error => this.dialogService.showInformationDialog("Failed to move task forward", error)
			);
	}

	moveToPreviousState(taskId: number) {
		let moveTaskDto = new MoveTaskDto();
		moveTaskDto.taskId = taskId;
		moveTaskDto.direction = 0;
		this.taskService.moveTask(moveTaskDto)
			.subscribe(
				success => { },
				error => this.dialogService.showInformationDialog("Failed to move task backwards", error)
			);
	}
}
