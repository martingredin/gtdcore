﻿

// Created with Typewriter (http://frhagn.github.io/Typewriter/)


export class UserModelDto {
    
    // USERID
    public userId: string = null;
    // PASSWORD
    public password: string = null;
    // CONFIRMPASSWORD
    public confirmPassword: string = null;

    constructor(data: any = null) {
        if (data !== null) {
            this.userId = data.userId;
            this.password = data.password;
            this.confirmPassword = data.confirmPassword;
        }
    }
}
