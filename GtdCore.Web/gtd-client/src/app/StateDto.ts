﻿

// Created with Typewriter (http://frhagn.github.io/Typewriter/)


export class StateDto {
    
    // ID
    public id: number = 0;
    // NAME
    public name: string = null;

    constructor(data: any = null) {
        if (data !== null) {
            this.id = data.id;
            this.name = data.name;
        }
    }
}
