export class Authentication {
    public userName: string;
    public token: string;
    public expirationDate: Date;

    constructor() {
        this.userName = "";
        this.token = "";
    }
}