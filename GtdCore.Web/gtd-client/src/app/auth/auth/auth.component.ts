import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { UserModelDto } from '../../UserModelDto';
import { DialogService } from '../../dialogs/dialog.service';

@Component({
	selector: 'auth',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
	public loginData: UserModelDto;

	constructor(
		private authService: AuthService,
		private router: Router,
		public dialogService: DialogService)
	{ }

	ngOnInit() {
		this.loginData = new UserModelDto();
	}

	login() {
		this.authService.login(this.loginData.userId, this.loginData.password)
			.subscribe(
				success => {
					this.router.navigateByUrl('/taskMgmt');
					this.loginData = new UserModelDto();
				},
				err => {
					this.dialogService.showInformationDialog("Login failed", err);
					this.loginData = new UserModelDto();
				}
			);
	}

	logout() {
		this.authService.logOut();
		this.router.navigateByUrl('/front');
	}

	loggedIn() {
		return this.authService.isLoggedIn();
	}
}
