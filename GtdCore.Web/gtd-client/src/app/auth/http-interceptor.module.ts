import { Injectable, NgModule } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthService } from './auth.service';
import { Authentication } from './authentication';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
	constructor(
		private authService: AuthService) { }

	intercept(req: HttpRequest<any>, next: HttpHandler):
		Observable<HttpEvent<any>> {

		if (!this.authService.isLoggedIn()) 
			return next.handle(req);

		const newReq = req.clone({
			headers: req.headers.set('Authorization', 'Bearer ' + this.authService.getToken())
		});
		return next.handle(newReq);
	}
};

@NgModule({
	providers: [{
		provide: HTTP_INTERCEPTORS,
		useClass: HttpRequestInterceptor,
		multi: true
	}]
})
export class HttpInterceptorModule { }
