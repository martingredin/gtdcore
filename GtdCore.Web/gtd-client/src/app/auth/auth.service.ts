import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { LocalStorageService } from 'ngx-webstorage';
import { Authentication } from './authentication';
import { Globals } from '../globals';
import { LoginDto } from '../LoginDto';
import { TokenDto } from '../TokenDto';
import { UserModelDto } from '../UserModelDto';
import { DialogService } from '../dialogs/dialog.service';

@Injectable({
	providedIn: 'root'
})
export class AuthService {
    private readonly loginUrl = Globals.BASE_API_URL + '/token';
    private readonly regUrl = Globals.BASE_API_URL + '/User';

    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

	constructor(
		private http: HttpClient,
		private localStorageService: LocalStorageService,
		private dialogService: DialogService)
	{ }

	createUser(userModelDto: UserModelDto) {
		return this.http
			.post(this.regUrl, userModelDto, this.httpOptions)
			.pipe(
				tap(res => console.log('success: ' + res)),
				catchError(this.handleError<any>('Authentication failed'))
			);
	}

	login(userName: string, password: string) {
        console.log("entering AuthService.login");
        console.log("loginUrl: " + this.loginUrl);
		var login = new LoginDto();
		login.username = userName;
		login.password = password;
        return this.http
            .post(this.loginUrl, login, this.httpOptions)
            .pipe(
				tap((tokenDto: TokenDto) => {
					console.log("fick ett token: " + tokenDto.token);
	                var authentication = new Authentication();
					authentication.userName = login.username;
					authentication.token = String(tokenDto.token);
					authentication.expirationDate = new Date(tokenDto.expiresIn);
					this.localStorageService.store(Globals.AUTH_KEY, authentication);
				}),
				catchError(this.handleError<string>("POST: " + this.loginUrl))
			);
    }

	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {

			//TODO: send the error to remote logging infrastructure
			console.log(error); // log to console instead

			//throw an error to run the error function in subscribe
			return throwError(error.error);
		};
	}

    logOut() {
        this.localStorageService.clear(Globals.AUTH_KEY);
    }

    isLoggedIn(): boolean {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;
		var now = new Date();
		if (authentication == null || now > new Date(authentication.expirationDate))
			return false;
        return true;
	}

	getToken() {
		var authentication = this.localStorageService.retrieve(Globals.AUTH_KEY) as Authentication;
		if (authentication)
			return authentication.token;

		return "";
	}
}
