﻿

// Created with Typewriter (http://frhagn.github.io/Typewriter/)


export class LoginDto {
    
    // USERNAME
    public username: string = null;
    // PASSWORD
    public password: string = null;

    constructor(data: any = null) {
        if (data !== null) {
            this.username = data.username;
            this.password = data.password;
        }
    }
}
