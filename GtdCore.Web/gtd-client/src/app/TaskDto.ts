﻿

// Created with Typewriter (http://frhagn.github.io/Typewriter/)


export class TaskDto {
    
    // ID
    public id: number = 0;
    // USERNAME
    public userName: string = null;
    // TASKNAME
    public taskName: string = null;
    // DESCRIPTION
    public description: string = null;
    // STATE
    public state: string = null;
    // STARTDATE
	public startDate: string = null;
    // ENDDATE
	public endDate: string = null;

    constructor(data: any = null) {
        if (data !== null) {
            this.id = data.id;
            this.userName = data.userName;
            this.taskName = data.taskName;
            this.description = data.description;
            this.state = data.state;
            this.startDate = data.startDate;
            this.endDate = data.endDate;
        }
    }
}
