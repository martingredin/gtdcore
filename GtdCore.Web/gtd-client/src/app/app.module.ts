import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule, Modal, bootstrap4Mode } from 'ngx-modialog/plugins/bootstrap';
import { NgDatepickerModule } from 'ng2-datepicker';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { DialogService } from './dialogs/dialog.service';
import { InformationDialogComponent } from './dialogs/information-dialog/information-dialog.component';
import { ErrorReportDialogComponent } from './dialogs/error-report-dialog/error-report-dialog.component';
import { RegistrationFormComponent } from './dialogs/registration-form/registration-form.component';
import { TaskCreateDialogComponent } from './dialogs/task-create-dialog/task-create-dialog.component';
import { TaskEditDialogComponent } from './dialogs/task-edit-dialog/task-edit-dialog.component';

import { AuthService } from './auth/auth.service';
import { AuthComponent } from './auth/auth/auth.component';
import { HttpInterceptorModule } from './auth/http-interceptor.module';

import { FrontPageComponent } from './front-page/front-page.component';
import { TaskMgmtComponent } from './task-mgmt/task-mgmt.component';
import { TaskService } from './task-mgmt/task.service';
import { StateMgmtComponent } from './state-mgmt/state-mgmt.component';
import { StateFilterPipe } from './state-mgmt/state-filter.pipe';

bootstrap4Mode();

@NgModule({
    declarations: [
        AppComponent,
        InformationDialogComponent,
        ErrorReportDialogComponent,
        RegistrationFormComponent,
        TaskCreateDialogComponent,
        TaskEditDialogComponent,
        FrontPageComponent,
        AuthComponent,
        TaskMgmtComponent,
        StateMgmtComponent,
        StateFilterPipe
    ],
    entryComponents: [
        InformationDialogComponent,
        ErrorReportDialogComponent,
        RegistrationFormComponent,
        TaskCreateDialogComponent,
        TaskEditDialogComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ModalModule.forRoot(),
        BootstrapModalModule,
        NgDatepickerModule,
		NgxWebstorageModule.forRoot(),
		AngularFontAwesomeModule,
		HttpInterceptorModule
    ],
    providers: [
        DialogService,
		AuthService,
		TaskService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
