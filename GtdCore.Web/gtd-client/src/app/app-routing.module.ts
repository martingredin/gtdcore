import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteGuard } from './auth/route.guard';
import { FrontPageComponent } from './front-page/front-page.component';
import { TaskMgmtComponent } from './task-mgmt/task-mgmt.component';
import { StateMgmtComponent } from './state-mgmt/state-mgmt.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: '/front',
		pathMatch: 'full'
	},
	{
		path: 'front',
		component: FrontPageComponent
	},
	{
		path: 'taskMgmt',
		component: TaskMgmtComponent,
		canActivate: [RouteGuard]
	},
	{
		path: 'stateMgmt',
		component: StateMgmtComponent,
		canActivate: [RouteGuard]
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
