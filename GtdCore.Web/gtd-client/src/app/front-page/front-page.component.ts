import { Component, OnInit } from '@angular/core';
//import { Overlay } from 'ngx-modialog';
//import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { AuthService } from '../auth/auth.service';
import { DialogService } from '../dialogs/dialog.service';
import { UserModelDto } from '../UserModelDto';

@Component({
	selector: 'app-front-page',
	templateUrl: './front-page.component.html',
	styleUrls: ['./front-page.component.css']
})
export class FrontPageComponent implements OnInit {

	constructor(
		private authService: AuthService,
		public dialogService: DialogService
	) { }

	ngOnInit() {
		
	}

	loggedIn() {
		return this.authService.isLoggedIn();
	}

	register() {
		this.dialogService.showRegDialog()
			.result.then(returnData => {
				this.authService.createUser(returnData)
					.subscribe(res => {
						this.dialogService.showInformationDialog(
							'Welcome to the Gtd community',
							'Thank you for joining our website, we hope youe life will become more efficient and productive with our help. An email with a confirmation link has been sent to the email address you supplied, please click the link to activate your registration before trying to login.')
					}), err => {
						this.dialogService.showInformationDialog(
							'Registration failed',
							'Error message: ' + err)
					}
			});
	}

	reportError() {
		this.dialogService.showErrorReportDialog()
			.result.then(dialog => {
				dialog.result.then(returnData => {
					//anropa errorreportservice (finns inte �n)
					//alert(
					//	'name: ' + returnData.name + '\n' +
					//	'phoneNumber: ' + returnData.phoneNumber + '\n' +
					//	'email: ' + returnData.email + '\n' +
					//	'description: ' + returnData.description);

				});
			});
	}
}
