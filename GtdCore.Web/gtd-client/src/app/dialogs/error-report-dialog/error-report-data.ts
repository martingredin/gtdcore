import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';

export class ErrorReportData extends BSModalContext {
  public name: string;
  public phoneNumber: string; //maska
  public email: string; //kolla format
  public description: string;
}
