import { Component, OnInit } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';
import { ErrorReportData } from './error-report-data';

@Component({
  selector: 'app-error-report-dialog',
  templateUrl: './error-report-dialog.component.html',
  styleUrls: ['./error-report-dialog.component.css']
})
export class ErrorReportDialogComponent implements CloseGuard, ModalComponent<ErrorReportData> {
  errorReportData: ErrorReportData;
  constructor(public dialog: DialogRef<ErrorReportData>) {
    //dialog.setCloseGuard(this);
    this.errorReportData = dialog.context;
  }

  ngOnInit() {
  }

  ok() {
    this.dialog.close(this.errorReportData);
  }

  cancel() {
    this.dialog.dismiss();
  }

  beforeDismiss(): boolean {
    return false;
  }

  beforeClose(): boolean {
    return false;
  }
}
