import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';

export class InformationDialogData extends BSModalContext {
  public caption: string;
  public description: string;
}
