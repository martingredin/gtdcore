import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';
import { InformationDialogData } from './information-dialog-data';

@Component({
  selector: 'app-information-dialog',
  templateUrl: './information-dialog.component.html',
  styleUrls: ['./information-dialog.component.css']
})
export class InformationDialogComponent implements CloseGuard, ModalComponent<InformationDialogData> {
  informationDialogData: InformationDialogData;
  constructor(public dialog: DialogRef<InformationDialogData>) {
    this.informationDialogData = dialog.context;
    dialog.setCloseGuard(this);
  }

  ngOnInit() {
  }

  close() {
    this.dialog.close();
  }

  beforeDismiss(): boolean {
    console.log('beforeDismiss');
    return false;
  }

  beforeClose(): boolean {
    console.log('beforeClose');
    return false;
  }
}
