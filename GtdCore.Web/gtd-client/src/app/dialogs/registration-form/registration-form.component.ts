import { Component, OnInit } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';
import { UserModelDto } from '../../UserModelDto';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements CloseGuard, ModalComponent<UserModelDto> {
  public registrationData: UserModelDto;

  constructor(public dialog: DialogRef<UserModelDto>) {
    dialog.setCloseGuard(this);
    this.registrationData = dialog.context;
  }

  ngOnInit() {
  }

  ok() {
    this.dialog.close(this.registrationData);
  }

  cancel() {
    this.dialog.dismiss();
  }

  beforeDismiss(): boolean {
    return false;
  }

  beforeClose(): boolean {
    return false;
  }

}
