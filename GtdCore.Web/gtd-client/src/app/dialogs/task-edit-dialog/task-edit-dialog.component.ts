import { Component, OnInit } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';
import { DatepickerOptions } from 'ng2-datepicker';
import * as svLocale from 'date-fns/locale/sv';
import { TaskDto } from '../../TaskDto';

@Component({
  selector: 'app-task-edit-dialog',
  templateUrl: './task-edit-dialog.component.html',
  styleUrls: ['./task-edit-dialog.component.css']
})
export class TaskEditDialogComponent implements CloseGuard, ModalComponent<TaskDto> {
    private taskDto: TaskDto;

    dpOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2030,
        displayFormat: 'YYYY-MM-DD',
        barTitleFormat: 'MMMM YYYY',
        dayNamesFormat: 'dd',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
        locale: svLocale,
        barTitleIfEmpty: 'Välj ett datum',
        placeholder: 'Välj ett datum', // HTML input placeholder attribute (default: '')
        addClass: 'form-control', // Optional, value to pass on to [ngClass] on the input field
        addStyle: {}, // Optional, value to pass to [ngStyle] on the input field
        fieldId: 'my-date-picker', // ID to assign to the input field. Defaults to datepicker-<counter>
        useEmptyBarTitle: true, // Defaults to true. If set to false then barTitleIfEmpty will be disregarded and a date will always be shown 
    };

    constructor(public dialog: DialogRef<TaskDto>) {
        this.taskDto = dialog.context;
        dialog.setCloseGuard(this);
    }

    ok() {
        //this.taskDto.startDate = this.taskDto.startDate + 'T00:00:00';
        //this.taskDto.endDate = this.taskDto.endDate + 'T00:00:00';
        this.dialog.close(this.taskDto);
    }

    cancel() {
        this.dialog.dismiss();
    }

    beforeDismiss(): boolean {
        return false;
    }

    beforeClose(): boolean {
        return false;
    }
}
