import { Injectable } from '@angular/core';
import { overlayConfigFactory, DialogRef, ModalComponent, CloseGuard } from 'ngx-modialog';
import { Modal, BSModalContext } from 'ngx-modialog/plugins/bootstrap';

import { InformationDialogComponent } from './information-dialog/information-dialog.component';
import { InformationDialogData } from './information-dialog/information-dialog-data';

import { ErrorReportDialogComponent } from './error-report-dialog/error-report-dialog.component';
import { ErrorReportData } from './error-report-dialog/error-report-data';

import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { UserModelDto } from '../UserModelDto';

import { TaskCreateDialogComponent } from './task-create-dialog/task-create-dialog.component'
import { TaskEditDialogComponent } from './task-edit-dialog/task-edit-dialog.component'
import { TaskDto } from '../TaskDto';

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  constructor(private modal: Modal) { }

  showInformationDialog(caption: string, description: string): DialogRef<InformationDialogData> {
    var data = new InformationDialogData();
    data.caption = caption;
    data.description = description;

    return this.modal.open(
      InformationDialogComponent,
      overlayConfigFactory(data, BSModalContext));
  }

  showErrorReportDialog(): DialogRef<ErrorReportData> {
    return this.modal.open(
      ErrorReportDialogComponent,
      overlayConfigFactory(new ErrorReportData, BSModalContext));
  }

  showRegDialog(): DialogRef<UserModelDto> {
    return this.modal.open(
      RegistrationFormComponent,
      overlayConfigFactory(new UserModelDto, BSModalContext));
    }

    showTaskCreateDialog(): DialogRef<TaskDto> {
        return this.modal.open(
            TaskCreateDialogComponent,
            overlayConfigFactory(new TaskDto, BSModalContext));
    }

    showTaskEditDialog(taskDto: TaskDto): DialogRef<TaskDto> {
        return this.modal.open(
            TaskEditDialogComponent,
            overlayConfigFactory(taskDto, BSModalContext));
    }
}
